﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;

namespace FlipperNet.Manager
{
    public class ConfigManager
    {
        private Configuration config;
        private Dictionary<String, String> values;
        private String fileName;
        private bool loaded = false;

        public ConfigManager()
        {
            String appData = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
            String folder = System.IO.Path.Combine(appData, "flippernet");

            if (!System.IO.Directory.Exists(folder))
            {
                System.IO.Directory.CreateDirectory(folder);
            }

            this.fileName = System.IO.Path.Combine(folder, "config.dat");

            if (System.IO.File.Exists(fileName))
            {
                ExeConfigurationFileMap configMap = new ExeConfigurationFileMap();
                configMap.ExeConfigFilename = this.fileName;
                this.config = ConfigurationManager.OpenMappedExeConfiguration(configMap, ConfigurationUserLevel.None);
                this.loaded = true;
            }
            else
            {
                this.config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            }

            this.values = new Dictionary<string, string>();
            this.Load();
        }

        public void Load()
        {
            foreach (String k in this.config.AppSettings.Settings.AllKeys)
            {
                this.SetValue(k, this.config.AppSettings.Settings[k].Value);
            }
        }

        public void Save()
        {
            foreach (KeyValuePair<String, String> val in this.values)
            {
                if (this.config.AppSettings.Settings.AllKeys.Contains(val.Key))
                {
                    this.config.AppSettings.Settings[val.Key].Value = val.Value;
                }
                else
                {
                    this.config.AppSettings.Settings.Add(val.Key, val.Value);
                }
            }

            if (this.loaded)
            {
                this.config.Save();
            }
            else
            {
                this.config.SaveAs(this.fileName, ConfigurationSaveMode.Full);
            }
        }

        public int GetValueInt(String key)
        {
            String o;

            if (this.values.TryGetValue(key, out o))
            {
                int r;
                if (int.TryParse(o, out r))
                {
                    return r;
                }
                else
                {
                    throw new ArgumentException("Specified key does not contains int value");
                }
            }
            else
            {
                throw new ArgumentOutOfRangeException("Config does not contains this key: " + key);
            }
        }

        public bool GetValueBool(String key)
        {
            String s = this.GetValueStr(key);
            return s.Equals("on", StringComparison.InvariantCultureIgnoreCase);
        }

        public String GetValueStr(String key)
        {
            String s;
            if (this.values.TryGetValue(key, out s))
            {
                return s;
            }
            else
            {
                return null;
            }
        }

        public IEnumerable<String> GetValueArrayStr(String key)
        {
            String v = this.GetValueStr(key);
            if (v != null)
            {
                foreach (String s in v.Split('|'))
                {
                    yield return s;
                }
            }
        }

        public IEnumerable<int> GetValueArrayInt(String key)
        {
            IEnumerable<String> v = this.GetValueArrayStr(key);
            if (v != null)
            {
                foreach (String s in v)
                {
                    int i;
                    if (!int.TryParse(s, out i))
                        throw new ArgumentException("Specified key does not contains int array: " + key);

                    yield return i;
                }
            }
        }

        public void SetValue(String key, String[] values)
        {
            StringBuilder sb = new StringBuilder(values.Length * 2);
            foreach (String s in values)
            {
                sb.Append(s.Trim());
                sb.Append('|');
            }

            this.SetValue(key, sb.ToString());
        }

        public void SetValue(String key, int[] values)
        {
            this.SetValue(key, values.Select((i) => i.ToString()).ToArray());
        }

        public void SetValue(String key, int value)
        {
            this.SetValue(key, value.ToString());
        }

        public void SetValue(String key, bool value)
        {
            this.SetValue(key, value ? "on" : "off");
        }

        public void SetValue(String key, String value)
        {
            if (this.values.ContainsKey(key))
            {
                this.values[key] = value;
            }
            else
            {
                this.values.Add(key, value);
            }
        }
    }
}
