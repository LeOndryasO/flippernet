﻿using QuickFont;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace FlipperNet.Manager
{
    public class FontManager : IDisposable
    {
        public const int BIG = 48;
        public const int SMALL = 32;

        private QFont defaultFont;
        private Dictionary<int, QFont> fonts;

        public FontManager()
        {
            this.fonts = new Dictionary<int, QFont>();
            this.LoadDefaults();
        }

        public void LoadDefaults()
        {
            this.GetFont(FontManager.BIG);
            this.defaultFont = this.GetFont(FontManager.SMALL);
        }

        public QFont GetFont()
        {
            return this.defaultFont;
        }

        public QFont GetFont(int size)
        {
            QFont font;
            if (fonts.TryGetValue(size, out font))
            {
                return font;
            }
            else
            {
                font = this.LoadFont("Resources/Fonts/thin_pixel-7.ttf", size);
                this.fonts.Add(size, font);
                return font;
            }
        }

        private QFont LoadFont(String path, int size)
        {
            if (!File.Exists(path))
            {
                throw new ArgumentException("Path does not exist!");
            }

            QFontBuilderConfiguration conf = new QFontBuilderConfiguration();
            conf.SuperSampleLevels = 1;
            conf.TextGenerationRenderHint = TextGenerationRenderHint.AntiAlias;

            QFont font = new QFont(path, size, conf);
            font.Options.LineSpacing = 1.5f;
            return font;
        }

        public void Dispose()
        {
            foreach (QFont f in this.fonts.Values)
            {
                f.Dispose();
            }
        }
    }
}
