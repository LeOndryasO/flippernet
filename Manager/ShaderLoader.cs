﻿using OpenTK;
using OpenTK.Graphics.OpenGL;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlipperNet.Manager
{
    public static class ShaderLoader
    {
        public static int? loadShader(String vsh, String fsh)
        {
            int program = 0;
            int vS = 0, fS = 0;

            try
            {
                vS = ShaderLoader.CreateShader(ShaderType.VertexShader, vsh);
                fS = ShaderLoader.CreateShader(ShaderType.FragmentShader, fsh);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return null;
            }

            program = GL.CreateProgram();

            if (vS == 0 || fS == 0 || program == 0)
            {
                return null;
            }

            GL.AttachShader(program, vS);
            GL.AttachShader(program, fS);
            GL.LinkProgram(program);

            int par = 0;

            GL.Arb.GetObjectParameter(program, ArbShaderObjects.ObjectLinkStatusArb, out par);
            if (par == 0)
            {
                Console.WriteLine(ShaderLoader.GetLogInfo(program));
                return null;
            }

            GL.ValidateProgram(program);
            GL.Arb.GetObjectParameter(program, ArbShaderObjects.ObjectValidateStatusArb, out par);
            if (par == 0)
            {
                Console.WriteLine(ShaderLoader.GetLogInfo(program));
                return null;
            }

            Console.WriteLine("Shaders loaded, program ID " + program + ", VS ID " + vS + ", FS ID " + fS);
            return program;
        }

        public static int GetUniformID(int? program, String uniform)
        {
            if(program != null)
                return GL.GetUniformLocation((int)program, uniform);

            return 0;
        }

        private static int CreateShader(ShaderType shaderType, String fileName) {
            int shader = 0;

            try
            {
                string shaderContent = File.ReadAllText(fileName);
                shader = GL.CreateShader(shaderType);

                if (shader == 0)
                {
                    return 0;
                }

                GL.ShaderSource(shader, shaderContent);
                GL.CompileShader(shader);

                int par = 0;
                GL.Arb.GetObjectParameter(shader, ArbShaderObjects.ObjectCompileStatusArb, out par);
                if (par == 0)
                {
                    throw new Exception("Error creating shader: " + ShaderLoader.GetLogInfo(shader));
                }

                return shader;
            }
            catch (Exception e)
            {
                GL.Arb.DeleteObject(shader);
                throw e;
            }
        }

        private static String GetLogInfo(int obj)
        {
            StringBuilder sb = null;
            int par = 0;
            int l = 0;
            GL.Arb.GetObjectParameter(obj, ArbShaderObjects.ObjectInfoLogLengthArb, out par);
            GL.Arb.GetInfoLog(obj, par, out l, sb);
           return sb.ToString();
        }
    }
}
