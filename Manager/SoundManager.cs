﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FlipperNet;
using Cgen.Audio;
using System.Threading.Tasks;

namespace FlipperNet.Manager
{
    public class SoundManager : IDisposable
    {
        private String audioDir = "Resources/Sounds/";

        private Dictionary<String, Sound> effects = new Dictionary<String, Sound>(16);
        private Dictionary<String, String> music = new Dictionary<String, String>(8);

        private Sound currentMusic = null;

        private bool isLoading = false;
        private bool enabled = true;
        private bool canPlay = true;

        public SoundSystem System { get; private set; }
        public MusicManager MusicManager { get; private set; }
        public bool IsLoading { get { return this.isLoading;  } }
        public bool CanPlay { get { return this.canPlay; } }

        public SoundManager()
        {
            this.System = SoundSystem.Instance();
            this.System.Init();
           
            this.LoadSounds();
            this.MusicManager = new MusicManager(this, ref music);
        }

        private void LoadSounds()
        {
            try
            {
                this.AddSound("coin", "coin.wav");
                this.AddSound("explosion", "explosion.wav");
                this.AddSound("hit", "hit.wav");
                this.AddSound("ice", "ice.wav");
                this.AddSound("jump", "jump.wav");
                this.AddSound("menu", "menu.wav");
                this.AddSound("select", "menugo.wav");
                this.AddSound("portal", "portal.wav");
            } catch {
                Console.WriteLine("Sounds could not be loaded, disabling sounds.");
                this.enabled = false;
                this.canPlay = false;
            }
        }

        public bool IsMusicPlaying()
        {
            return (this.currentMusic != null && (this.currentMusic.GetStatus() != SoundChannel.Status.Stopped));
        }

        public void PlaySound(String name, float pitch, float volume)
        {
            if (this.canPlay)
            {
                Sound s = this.effects[name];
                if (s != null)
                {
                    s.SetVolume(volume * 100);
                    s.SetPitch(pitch);
                    s.Play();
                }
            }
        }

        public void PlaySound(String name, float pitch)
        {
            this.PlaySound(name, pitch, 1);
        }

        
        public void PlayMusic(String name, float volume)
        {
            if (this.canPlay && !this.isLoading)
            {
                this.StopMusic();
                this.isLoading = true;
                Task t = new Task(() => this.LoadAndPlay(name, volume));
                t.Start();
            }
        }

        private void LoadAndPlay(String name, float volume)
        {
            lock (this.audioDir)
            {
                this.currentMusic = new Sound(name, this.audioDir + this.music[name]);
                this.currentMusic.SetVolume(volume * 100);
                this.currentMusic.Play();
                this.isLoading = false;
            }
        }

        private void WaitAndStop()
        {
            lock (this.audioDir)
            {
                while (this.isLoading) ;
                this.StopMusic();
            }
        }

        public void PlayMusic(String name)
        {
            this.PlayMusic(name, 1);
        }

        public void PlayMusic()
        {
            if (this.currentMusic != null)
            {
                if (this.currentMusic.GetStatus() != SoundChannel.Status.Playing)
                {
                    this.currentMusic.Play();
                }
            }
        }

        public void PauseMusic()
        {
            if (this.currentMusic != null)
            {
                if (this.currentMusic.GetStatus() == SoundChannel.Status.Playing)
                {
                    this.currentMusic.Pause();
                }
            }
        }

        public void StopMusic()
        {
            if (this.currentMusic != null)
            {
                this.currentMusic.Stop();
                this.currentMusic.Dispose();
                this.currentMusic = null;
            }
        }

        public void SetSoundEnabled(bool e)
        {
            if (this.enabled)
            {
                this.canPlay = e;

                if (e == false)
                {
                    foreach (Sound s in this.effects.Values)
                    {
                        if(s.GetStatus() == SoundChannel.Status.Playing)
                            s.Stop();
                    }

                    if (this.isLoading)
                    {
                        Task.Factory.StartNew(() => { this.WaitAndStop(); });
                    }

                    this.PauseMusic();
                }
                else
                {
                    this.PlayMusic();
                }
            }
        }

        private void AddSound(String name, String file)
        {
            this.effects.Add(name, new Sound(name, this.audioDir + file));
        }

        public void Dispose()
        {
            foreach (Sound s in this.effects.Values)
            {
                s.Dispose();
            }

            this.System.Dispose();
        }
    }
}
