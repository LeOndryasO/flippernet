﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenTK.Graphics.OpenGL;
using System.Drawing;
using System.Drawing.Imaging;
using OpenTK;

namespace FlipperNet.Manager
{
    public class Texture
    {
        public int TextureID { get; set; }
        public Rectangle Size { get; set; }
        public Vector2 SheetSize { get; set; }
        public Vector2 Power2Size { get; set; }
    }

    public class TextureManager : IDisposable
    {
        private Dictionary<int, Texture> textures;

        private String texturesDir = "Resources/Tex/";

        private int lastBindID = -1;

        public TextureManager()
        {
            this.textures = new Dictionary<int, Texture>();
            this.LoadTextures();
        }

        public Texture GetTexture(int id)
        {
            return this.textures[id];
        }

        public void LoadTextures()
        {
            this.AddTexture(0, "logo.png");

            this.AddTexture(1, "blocks2.png");
            this.SetSpritesheet(1, new Vector2(16, 16));

            this.AddTexture(2, "coins.png");
            this.SetSpritesheet(2, new Vector2(32, 32));

            this.AddTexture(3, "portal.png");
            this.SetSpritesheet(3, new Vector2(16, 16));

            this.AddTexture(5, "life_full.png");
            this.AddTexture(6, "life_null.png");

            this.AddTexture(7, "pl1.png");
            this.SetSpritesheet(7, new Vector2(32, 32));

            this.AddTexture(8, "enemy1.png");
            this.SetSpritesheet(8, new Vector2(16, 16));

            this.AddTexture(9, "enemy2.png");
            this.SetSpritesheet(9, new Vector2(16, 16));
        }

        public void SetSpritesheet(int id, Vector2 size)
        {
            Texture t = this.textures[id];
            t.SheetSize = size;
        }

        public void AddTexture(int id, String file)
        {
            Rectangle r;
            int tID = this.LoadTexture(this.texturesDir + file, out r);

            Texture t = new Texture();

            t.TextureID = tID;
            t.Size = r;
            t.Power2Size = new Vector2(this.Get2Fold(r.Width), this.Get2Fold(r.Height));

            this.textures.Add(id, t);
            Console.WriteLine("Loaded texture " + file);
        }

        public void Dispose()
        {
            try
            {
                foreach (Texture t in this.textures.Values)
                {
                    GL.DeleteTexture(t.TextureID);
                }

                GL.DeleteTexture(GLUtil.colorTextureID);
                GL.Ext.DeleteFramebuffers(1, ref GLUtil.framebufferID);
                GL.Ext.DeleteRenderbuffers(1, ref GLUtil.depthRenderBufferID);
            }
            catch
            {
                Console.WriteLine("Exception when disposing OpenGL. Poor little memory of yours.");
            }
        }

        public void Bind(int id)
        {
            int tID = this.textures[id].TextureID;

            if (tID != this.lastBindID)
            {
                this.lastBindID = tID;
                GL.Enable(EnableCap.Texture2D);
                GL.BindTexture(TextureTarget.Texture2D, tID);
            }
        }

        public void ForceBindedID(int tID)
        {
            this.lastBindID = tID;
        }

        public void ClearBind()
        {
            this.lastBindID = -1;
            GL.BindTexture(TextureTarget.Texture2D, 0);
        }

        private int LoadTexture(string filename, out Rectangle rect)
        {
            if (String.IsNullOrEmpty(filename))
                throw new ArgumentException(filename);
            GL.Enable(EnableCap.Texture2D);

            int id;
            GL.GenTextures(1, out id);
            GL.BindTexture(TextureTarget.Texture2D, id);

            Bitmap bmp = new Bitmap(filename);
            rect = new Rectangle(0, 0, bmp.Width, bmp.Height);
            BitmapData bmp_data = bmp.LockBits(rect, ImageLockMode.ReadOnly, System.Drawing.Imaging.PixelFormat.Format32bppArgb);

            GL.TexImage2D(TextureTarget.Texture2D, 0, PixelInternalFormat.Rgba, bmp_data.Width, bmp_data.Height, 0,
                OpenTK.Graphics.OpenGL.PixelFormat.Bgra, PixelType.UnsignedByte, bmp_data.Scan0);

            bmp.UnlockBits(bmp_data);

            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (int)TextureMinFilter.Nearest);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (int)TextureMagFilter.Nearest);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapS, (int)TextureWrapMode.ClampToEdge);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapT, (int)TextureWrapMode.ClampToEdge);

            return id;
        }

        public float GetX1(int id, int image)
        {
            Texture t = this.textures[id];
            float pos = (this.textures[id].SheetSize.X * (image - 1));
            float posFloat = (pos / t.Size.Width) /** ((float)t.Size.Width / t.Power2Size.X)*/;

            return posFloat;
        }

        public float GetX2(int id, int image)
        {
            Texture t = this.textures[id];
            float pos = (t.SheetSize.X * (image));
            float posFloat = pos / t.Size.Width /** ((float)t.Size.Width / t.Power2Size.X)*/;

            return posFloat;
        }

        private int Get2Fold(int fold)
        {
            int ret = 2;
            while (ret < fold)
            {
                ret *= 2;
            }

            return ret;
        }
    }
}
