﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FlipperNet.Manager
{
    public class MusicManager
    {
        private String[] gameMusic;
        private String[] menuMusic;

        private String[] menuQueue;
        private String[] gameQueue;

        private int menuIndex = 0;
        private int gameIndex = 0;

        private readonly int startGameMusicProbability = 10;

        private SoundManager sound;
        private Random rand = new Random();

        private bool isGame = false;
        private bool wasInGame = false;

        private Dictionary<String, String> music;

        public MusicManager(SoundManager soundManager, ref Dictionary<String, String> musicDict)
        {
            this.sound = soundManager;
            this.music = musicDict;
            this.LoadMusic();
            this.SortMusic();
        }

        private void LoadMusic()
        {
            this.music.Add("menu1", "stabs.ogg");
            this.music.Add("game1", "hdo.ogg");
            this.music.Add("game2", "kni.ogg");
            this.music.Add("game3", "vlp.ogg");
        }

        private void SortMusic()
        {
            this.gameMusic = new String[] { "game1", "game2", "game3" };
            this.gameQueue = new String[this.gameMusic.Length];
            foreach (String music in this.gameMusic)
            {
                this.AddMusicToField(ref this.gameQueue, music);
            }

            this.menuMusic = new String[] { "menu1" };
            this.menuQueue = new String[this.menuMusic.Length];
            foreach (String music in this.menuMusic)
            {
                this.AddMusicToField(ref this.menuQueue, music);
            }
        }

        private void AddMusicToField(ref String[] queue, String name)
        {
            int i = this.rand.Next(queue.Length);

            if (queue[i] != null)
            {
                this.AddMusicToField(ref queue, name);
            }
            else
            {
                queue[i] = name;
            }
        }

        public void UpdateMusic(bool isInGame)
        {
            if (!this.sound.IsLoading)
            {
                this.wasInGame = this.isGame;
                this.isGame = isInGame;
            }

            if (this.isGame ^ this.wasInGame)
            {
                this.sound.StopMusic();
                return;
            }

            if (!this.sound.IsMusicPlaying())
            {
                if (this.isGame)
                {
                    if (this.rand.Next(this.startGameMusicProbability) == 1)
                    {
                        this.sound.PlayMusic(gameQueue[gameIndex], 0.2f);

                        this.gameIndex++;

                        if (this.gameIndex > (this.gameQueue.Length - 1))
                        {
                            this.gameIndex = 0;
                        }
                    }
                }
                else
                {
                    this.sound.PlayMusic(this.menuQueue[this.menuIndex], 0.9f);

                    this.menuIndex++;

                    if (this.menuIndex > (this.menuQueue.Length - 1))
                    {
                        this.menuIndex = 0;
                    }

                }
            }
        }

    }
}
