﻿using FlipperNet.Worlds;
using FlipperNet.Worlds.Block;
using FlipperNet.Worlds.Entity;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;

namespace FlipperNet
{
    public class WorldLoader
    {
        private readonly String LEVEL_FOLDER = "Resources/Level/";
        private readonly String LEVEL_PLACEHOLDER = "l{0}w{1}.png";
        private readonly String LEVEL_HINTS_PLACEHOLDER = "l{0}ht.dat";
        private readonly String LEVEL_FILTER = "l*w*.png";

        private Game game;
        private List<Level> loaded;
        private Random rnd;

        public WorldLoader(Game game)
        {
            this.game = game;
            this.rnd = new Random();
        }

        public List<Level> GetLevels()
        {
            return this.loaded;
        }

        public void LoadLevels()
        {
            this.loaded = new List<Level>(4);
            int levelCount = 0;

            foreach (String path in Directory.EnumerateFiles(this.LEVEL_FOLDER, this.LEVEL_FILTER))
            {
                int level;
                if (int.TryParse(Path.GetFileNameWithoutExtension(path).Substring(1, 1), out level))
                {
                    if (level > levelCount)
                    {
                        levelCount = level;
                    }
                }
                else
                {
                    throw new IOException("Wrong level name (this should NEVER happen!)");
                }

            }

            for (int i = 1; i <= levelCount; i++)
            {
                Console.WriteLine("Loading level " + i);
                this.loaded.Add(this.LoadLevel(i));
            }
        }

        private Level LoadLevel(int id)
        {
            Level l = new Level(id);
            Color[] comb = this.GetColorCombination(null);

            l.SetWorld(0, this.LoadWorld(Path.Combine(this.LEVEL_FOLDER,
                String.Format(this.LEVEL_PLACEHOLDER, id, 1))));
            l.GetWorld(0).BackgroundBottom = comb[0];
            l.GetWorld(0).BackgroundTop = comb[1];

            comb = this.GetColorCombination(comb);

            l.SetWorld(1, this.LoadWorld(Path.Combine(this.LEVEL_FOLDER,
               String.Format(this.LEVEL_PLACEHOLDER, id, 2))));
            l.GetWorld(1).BackgroundBottom = comb[0];
            l.GetWorld(1).BackgroundTop = comb[1];

            this.LoadHints(l);
            return l;
        }

        private void LoadHints(Level level)
        {
            String path = Path.Combine(this.LEVEL_FOLDER, String.Format(this.LEVEL_HINTS_PLACEHOLDER, level.ID));
            using (StreamReader sr = new StreamReader(path))
            {
                while (!sr.EndOfStream)
                {
                    String l = sr.ReadLine();
                    String[] parts = l.Split(';');

                    int fromX = int.Parse(parts[0]);
                    int toX = int.Parse(parts[1]);
                    int world = int.Parse(parts[2]);

                    Hint h = ((world == -1) ? new Hint(parts[3], fromX, toX, level)
                    : new Hint(parts[3], fromX, toX, level, world));

                    level.Hints.Add(h);
                }
            }
        }

        private World LoadWorld(String path)
        {
            Bitmap img = new Bitmap(Image.FromFile(path));
            int w = img.Size.Width;
            int h = img.Size.Height;

            FlipperNet.Worlds.World newWorld = new FlipperNet.Worlds.World(this.game, w, h);
            for (int y = 0; y < h; y++)
            {
                for (int x = 0; x < w; x++)
                {
                    Color color = img.GetPixel(x, y);
                    int blockID = Blocks.GetBlockIDForColor(color.ToArgb());
                    if (blockID == -1)
                    {
                        this.CreateEntity(newWorld, x, y, color.ToArgb());
                    }
                    else
                    {
                        newWorld.SetBlock(x, y, blockID);
                    }
                }
            }

            return newWorld;
        }

        private void CreateEntity(FlipperNet.Worlds.World world, int x, int y, int color)
        {
            switch ((uint)color)
            {
                case 0xFF123456:
                    world.AddEntity(new FollowingEnemy(x, y));
                    break;
                case 0xFF654321:
                    world.AddEntity(new WalkingEnemy(x, y));
                    break;
                case 0xFFFF0000:
                    world.CreatePlayer(x, y);
                    break;
            }
        }

        Color[][] combs = new Color[][] 
        {
			new Color[] { Color.FromArgb(140, 28, 28), Color.FromArgb(255, 187, 0) },
			new Color[] { Color.FromArgb(0, 143, 245), Color.FromArgb(143, 216, 255) },
			new Color[] { Color.FromArgb(0, 153, 17), Color.FromArgb(177, 247, 0) },
			new Color[] { Color.FromArgb(219, 190, 0), Color.FromArgb(255, 238, 0) } 
        };

        private Color[] GetColorCombination(Color[] not)
        {
            Color[] r = this.combs[rnd.Next(combs.Length)];

            if (r.Equals(not))
            {
                return this.GetColorCombination(not);
            }

            return r;
        }
    }
}
