﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenTK.Graphics.OpenGL;

namespace FlipperNet.Worlds.Entity
{
    public class Enemy : BaseEntity
    {
        protected float size = 1.0f;
        protected int texID = 8;
        protected int image = 1;

        protected float velX, velY;
        protected float speed = 0.05f;

        protected int hurtAmount = 20;
        protected float hurtTimer = 5;

        private int timer;

        public Enemy(int x, int y, float size)
            : base(new AABB(x, y - 1, x + size, y - 1 + size))
        {
            this.size = size;
        }

        public override void BeforeRender(float ptt)
        {
            this.RenderBB = AABB.Mix(this.lastBB, this.BB, ptt);
        }

        public override void Update(World world)
        {
            this.lastBB = this.BB.Clone();
            this.timer++;
            if (this.timer >= 2)
            {
                this.image++;

                if (this.image > 2)
                {
                    this.image = 1;
                }

                this.timer = 0;
            }

            this.Move(world);

            if (world.Player.BB.Intersects(this.BB))
            {
                this.hurtTimer++;
                if (this.hurtTimer >= 5)
                {
                    world.Player.Hurt(this.hurtAmount);
                    world.Game.GetSoundManager().PlaySound("hit", 1);
                    this.hurtTimer = 0;
                }
            }

            this.velX *= 0.8f;
            this.velY += 0.1f;
            this.velY *= 0.8f;

            this.ClipMovement(this.BB.MoveCollide(world, this.velX, this.velY));
        }

        public override void Render(World world, float ptt)
        {
            int bs = world.Game.BlockSize;

            GL.PushMatrix();
            GL.Translate(this.size * bs, 0, 0);

            GLUtil.DrawAtlasTexture(world.Game.GetTextureManager(), this.texID, this.image,
                (this.RenderBB.x0 * bs),
                (this.RenderBB.x1 * bs),
                (this.RenderBB.y0 * bs + bs),
                (this.RenderBB.y1 * bs + bs));

            GL.PopMatrix();
        }

        protected virtual void Move(World world) { }
        protected virtual void ClipMovement(float[] clip)
        {
            if (clip[1] != this.velY)
            {
                this.velY = 0;
            }
        }
    }
}
