﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FlipperNet.Worlds.Entity
{
    public class FollowingEnemy : Enemy
    {
        public FollowingEnemy(int x, int y) : base(x, y, 1f) { }

        protected override void Move(World world)
        {
            Player p = world.Player;
            float dis = p.BB.x0 - this.BB.x0;
            if ((dis < 10) && (dis > -10))
            {
                if ((p.BB.x0) > this.BB.x0)
                {
                    this.velX += this.speed;
                }
                else
                {
                    this.velX -= this.speed;
                }
            }
        } 
    }
}
