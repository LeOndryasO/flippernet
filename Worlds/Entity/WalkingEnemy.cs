﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FlipperNet.Worlds.Entity
{
    public class WalkingEnemy : Enemy
    {
        private bool facingRight = true;

        public WalkingEnemy(int x, int y)
            : base(x, y, 1.0f)
        {
            this.hurtAmount = 30;
            this.texID = 9;
            
            this.speed += ((float)Game.rnd.NextDouble() / 10);
            Console.WriteLine(speed);
        }

        protected override void Move(World world)
        {
            if (this.facingRight)
            {
                this.velX += this.speed;
            }
            else
            {
                this.velX -= this.speed;
            }
        }

        protected override void ClipMovement(float[] clip)
        {
            base.ClipMovement(clip);

            if (clip[0] != this.velX)
            {
                this.velX = 0;
                this.facingRight = !this.facingRight;
            }
        }
    }
}
