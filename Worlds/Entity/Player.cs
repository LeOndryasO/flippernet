﻿using OpenTK;
using OpenTK.Input;
using OpenTK.Graphics.OpenGL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FlipperNet.Worlds.Block;

namespace FlipperNet.Worlds.Entity
{
    public class Player : BaseEntity
    {
        public Player(int x, int y) : base(new AABB(x, y - 2, x + 0.5f, y)) { }

        private int texID = 7;
        private int currentImage = 1;
        private int groundImgStart = 2, groundImgEnd = 9, groundImgStand = 5;

        private float speed = 0.25f, jumpForce = 0.8f;
        private float velX, velY;

        private bool facingRight = true;
        private bool onGround = true;

        private float timer;
        private Dictionary<Vector2, int?> states = new Dictionary<Vector2, int?>();
        private int coins = 0;

        public bool OnPortal { get; private set; }
        public bool OnExit { get; private set; }

        public override void BeforeRender(float ptt)
        {
            this.RenderBB = AABB.Mix(this.lastBB, this.BB, ptt);
        }

        public override void Update(World world)
        {
            this.lastBB = this.BB.Clone();
            this.OnPortal = false;
            this.OnExit = false;
            this.CheckBlocks(world);
            var state = OpenTK.Input.Keyboard.GetState();

            if (state[Key.A] || state[Key.Left])
            {
                this.velX -= speed;
                this.facingRight = true;
            }

            if (state[Key.D] || state[Key.Right])
            {
                this.velX += speed;
                this.facingRight = false;
            }

            if (state[Key.W] || state[Key.Space] || state[Key.Up])
            {
                if (this.onGround)
                {
                    this.velY -= this.jumpForce;
                    this.onGround = false;
                    world.Game.GetSoundManager().PlaySound("jump", 1);
                }
                else if (Game.GodMode)
                {
                    this.velY -= 0.15f;
                }
            }

            if (Game.GodMode && (state[Key.S] || state[Key.Down]))
            {
                this.velY += 0.15f;
            }

            if (Game.GodMode && state[Key.N])
            {
                this.OnPortal = true;
            }

            if (Game.GodMode && state[Key.M])
            {
                this.OnExit = true;
            }

            this.velX *= 0.725f;

            if(!Game.GodMode)
                this.velY += 0.1f;

            if (Math.Abs(this.velX) > 0.01)
            {
                this.timer += this.onGround ? 1 : 0.2f;
            }
            else
            {
                this.currentImage = this.groundImgStand;
            }

            if (this.timer >= 2)
            {
                this.timer = 0;
                this.currentImage++;
                if (this.currentImage > this.groundImgEnd)
                {
                    this.currentImage = this.groundImgStart;
                }
            }

            float[] clip = this.BB.MoveCollide(world, this.velX, this.velY);


                if (this.velY != clip[1])
                {
                    this.onGround = velY > 0;
                    this.velY = 0;
                }
                else
                {
                    this.onGround = false;
                }

                if (this.velX != clip[0])
                {
                    this.velX = 0;
                }

                if (this.BB.y0 > world.Size.Y)
                {
                    base.Hurt(1f);
                }
            
        }

        public override void Render(World w, float ptt)
        {
            int bs = w.Game.BlockSize;

            GL.PushMatrix();
            GL.Translate(+0.5f * bs, 0, 0);

            if (!this.facingRight)
            {
                GLUtil.DrawAtlasTexture(w.Game.GetTextureManager(), this.texID,
                    this.currentImage, (int)(base.RenderBB.x0 * bs - bs * 0.25f),
                    (int)(base.RenderBB.x1 * bs + bs + bs * 0.25f),
                    (int)(base.RenderBB.y0 * bs + bs),
                    (int)(base.RenderBB.y1 * bs + bs));
            }
            else
            {
                GLUtil.DrawAtlasTexture(w.Game.GetTextureManager(), this.texID,
                    this.currentImage, (int)(base.RenderBB.x1 * bs + bs + bs * 0.25f),
                    (int)(base.RenderBB.x0 * bs - bs * 0.25f),
                    (int)(base.RenderBB.y0 * bs + bs),
                    (int)(base.RenderBB.y1 * bs + bs));
            }

            GL.PopMatrix();
        }

        public override bool ShouldRemove()
        {
            return Game.GodMode ? false : base.ShouldRemove();
        }

        public void SetVelocityZero()
        {
            this.velX = 0;
        }

        public int GetCoinsFlush()
        {
            int r = this.coins;
            this.coins = 0;
            return r;
        }

        private void UpdateIce(int x, int y, World w)
        {
            Vector2 c = new Vector2(x, y);
            int? st;
            this.states.TryGetValue(c, out st);
            int newSt;

            if (st == null)
            {
                this.states.Add(c, 10);
                return;
            }
            else
            {
                newSt = (int)st - 1;
                this.states[c] = newSt;
            }

            if (newSt <= 0)
            {
                this.states.Remove(c);
                w.SetBlock(x, y, 0);
                w.Game.GetSoundManager().PlaySound("ice", 1);
            }
        }

        private void CheckBlocks(World world)
        {
            int underX = (int)(this.BB.x1 + 0.5);
            int underY = (int)(this.BB.y1 + 1);
            int blockUnder = world.GetBlock(underX, underY);

            if (blockUnder == Blocks.bouncy.ID)
            {
                this.jumpForce = 1.8f;
            }
            else
            {
                this.jumpForce = 0.8f;
            }

            if (blockUnder == Blocks.ice.ID)
            {
                this.UpdateIce(underX, underY, world);
            }

            int minX = (int)(this.BB.x0) + 1;
            int maxX = (int)(this.BB.x1 + 1f) + 1;

            int minY = (int)this.BB.y0 + 1;
            int maxY = (int)this.BB.y1 + 2;

            for (int x = minX; x < maxX; x++)
            {
                for (int y = minY; y < maxY; y++)
                {
                    int blockID = world.GetBlock(x, y);

                    if (blockID == Blocks.coin.ID)
                    {
                        world.SetBlock(x, y, 0);
                        this.coins++;
                        world.Game.GetSoundManager().PlaySound("coin", 1);
                    }

                    if (blockID == Blocks.tnt.ID)
                    {
                        world.SetBlock(x, y, 0);
                        this.Hurt(0.5f);
                        world.Game.GetSoundManager().PlaySound("explosion", 1);
                    }

                    if (blockID == Blocks.portal.ID)
                    {
                        this.OnPortal = true;
                    }

                    if (blockID == Blocks.exit.ID)
                    {
                        this.OnExit = true;
                    }
                }
            }
        }
    }
}
