﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FlipperNet.Worlds.Entity
{
    public abstract class BaseEntity
    {
        protected AABB lastBB;
        protected float lifes;
        private bool isDead = false;

        public AABB BB { get; protected set; }
        public AABB RenderBB { get; protected set; }
        public bool IsColliding { get; set; }
        public float Lifes
        {
            get
            {
                return this.lifes;
            }

            set
            {
                if (value > 1)
                {
                    this.lifes = 1;
                }
                else
                {
                    this.lifes = value;
                }
            }
        }

        public BaseEntity(AABB bb)
        {
            this.BB = bb;
            this.lastBB = bb;
            this.RenderBB = bb;
            this.lifes = 1;
        }

        public BaseEntity(int x, int y, int width, int height) :
            this(new AABB(x, y, x + width, y + height)) { }

        public void Hurt(float dmg)
        {
            this.lifes -= dmg;

            if (this.lifes <= 0)
            {
                this.lifes = 0;
                this.isDead = true;
            }
        }

        public void Hurt(int dmg)
        {
            this.Hurt(dmg / 100f);
        }

        public virtual bool ShouldRemove()
        {
            return this.isDead;
        }

        public abstract void BeforeRender(float ptt);
        public abstract void Update(World world);
        public abstract void Render(World world, float ptt);
    }
}
