﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FlipperNet.Worlds
{
    public class Level
    {
        private World[] worlds = new World[2];
        private int levelID;
        private List<Hint> hints = new List<Hint>();

        public List<Hint> Hints
        {
            get
            {
                return this.hints;
            }
        }

        public int ID
        {
            get
            {
                return this.levelID;
            }
        }

        public Level(int id)
        {
            this.levelID = id;
        }

        public World GetWorld(int id)
        {
            return this.worlds[id];
        }

        public void SetWorld(int id, World w)
        {
            this.worlds[id] = w;
        }

        public override int GetHashCode()
        {
            return (this.levelID * 13 + (this.worlds[0] == null ? 1 : this.worlds[0].GetHashCode())
                + (this.worlds[1] == null ? 1 : this.worlds[1].GetHashCode()));
        }

        public override bool Equals(object obj)
        {
            if (obj is Level)
            {
                Level l = (Level)obj;
                return (l.levelID == this.levelID && l.GetWorld(0) == this.GetWorld(0) &&
                    l.GetWorld(1) == this.GetWorld(1));
            }

            return false;
        }
    }
}
