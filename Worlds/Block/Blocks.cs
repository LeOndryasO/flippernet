﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FlipperNet.Worlds.Block
{
    public static class Blocks
    {
        public static readonly Block grass = new Block(1).SetTexID(1).SetSpritesheetID(7).SetSaveColor(0xFF000000);
        public static readonly Block coin = new AnimatedBlock(2, 8).SetTicksForChange(5).SetTexID(2).SetCollidable(false).SetSaveColor(0xFFABCDEF);
        public static readonly Block bouncy = new Block(3).SetTexID(1).SetSpritesheetID(9).SetSaveColor(0xFF00FF00);
        public static readonly Block help = new Block(4).SetTexID(1).SetSpritesheetID(10).SetCollidable(false).SetSaveColor(0xFFFFFF00);
        public static readonly Block portal = new AnimatedBlock(5, 4).SetTexID(3).SetCollidable(false).SetSaveColor(0xFF0000FF);
        public static readonly Block exit = new Block(6).SetTexID(1).SetSpritesheetID(8).SetCollidable(false).SetSaveColor(0xFFFF00FF);
        public static readonly Block ice = new Block(7).SetTexID(1).SetSpritesheetID(11).SetSaveColor(0xFF00FFD2);
        public static readonly Block tnt = new Block(8).SetTexID(1).SetSpritesheetID(12).SetCollidable(false).SetSaveColor(0xFFFF8000);
        public static readonly Block brick = new Block(9).SetTexID(1).SetSpritesheetID(21).SetSaveColor(0xFFFF9A00);
        public static readonly Block stone = new Block(10).SetTexID(1).SetSpritesheetID(22).SetSaveColor(0xFF4C4C4C);

        private static Block[] blocks;
        private static int[] blockColors;

        public static void RegisterBlock(int id, Block b)
        {
            if (Blocks.blocks == null)
                Blocks.blocks = new Block[32];

            Blocks.blocks[id] = b;
        }

        public static void RegisterColor(int id, int color)
        {
            if (Blocks.blockColors == null)
                Blocks.blockColors = new int[32];

            Blocks.blockColors[id] = color;
        }

        public static Block GetBlock(int id)
        {
            return Blocks.blocks[id];
        }

        public static int GetBlockIDForColor(int color)
        {
            for (int i = 0; i < Blocks.blockColors.Length; i++)
            {
                if (Blocks.blockColors[i] == color)
                {
                    return i;
                }
            }

            return -1;
        }

        public static Block[] GetAllBlocks()
        {
            return Blocks.blocks;
        }
    }
}
