﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FlipperNet.Worlds.Block
{
    public class Block
    {
        public int ID { get; private set; }
        public int TextureID { get; set; }
        public int SpritesheetID { get; set; }
        private int _saveColor;
        public int SaveColor
        {
            get
            {
                return this._saveColor;
            }

            set
            {
                this._saveColor = value;
                Blocks.RegisterColor(this.ID, value);
            }
        }
        public bool Collidable { get; set; }

        public Block(int id)
        {
            this.ID = id;
            this.Collidable = true;
            Blocks.RegisterBlock(id, this);
        }

        public Block SetTexID(int id)
        {
            this.TextureID = id;
            return this;
        }

        public Block SetSpritesheetID(int id)
        {
            this.SpritesheetID = id;
            return this;
        }

        public Block SetSaveColor(uint c)
        {
            this.SaveColor = (int)c;
            return this;
        }

        public Block SetCollidable(bool c)
        {
            this.Collidable = c;
            return this;
        }

        public virtual void Update(World world) { }

    }
}
