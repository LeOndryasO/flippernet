﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FlipperNet.Worlds.Block
{
    public class AnimatedBlock : Block
    {
        private int inSheet, ticks = 0, ticksChange = 10;

        public AnimatedBlock(int id, int imagesInSheet) : base(id)
        {
            this.inSheet = imagesInSheet;
        }

        public AnimatedBlock SetTicksForChange(int t)
        {
            this.ticksChange = t;
            return this;
        }

        public override void Update(World world)
        {
            this.ticks++;
            if (this.ticks > this.ticksChange)
            {
                this.ticks = 0;
                this.SpritesheetID++;
                if (this.SpritesheetID > this.inSheet)
                {
                    this.SpritesheetID = 1;
                }
            }
        }
    }
}
