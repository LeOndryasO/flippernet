﻿using FlipperNet.Worlds.Block;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FlipperNet.Worlds
{
    public class AABB
    {
        private float eps = 0.0f;
        public float x0, y0, x1, y1;

        public static AABB Mix(AABB bb0, AABB bb1, float t)
        {
            float deltaX0 = bb1.x0 - bb0.x0;
            float deltaY0 = bb1.y0 - bb0.y0;
            float deltaX1 = bb1.x1 - bb0.x1;
            float deltaY1 = bb1.y1 - bb0.y1;

            float x0 = bb0.x0 + deltaX0 * t;
            float y0 = bb0.y0 + deltaY0 * t;
            float x1 = bb0.x1 + deltaX1 * t;
            float y1 = bb0.y1 + deltaY1 * t;

            return new AABB(x0, y0, x1, y1);
        }

        public AABB(float x0, float y0, float x1, float y1)
        {
            this.x0 = x0;
            this.x1 = x1;
            this.y0 = y0;
            this.y1 = y1;
        }

        public AABB Clone()
        {
            return new AABB(this.x0, this.y0, this.x1, this.y1);
        }

        public AABB Expand(float xa, float ya)
        {
            float _x0 = this.x0;
            float _y0 = this.y0;
            float _x1 = this.x1;
            float _y1 = this.y1;
            if (xa < 0.0F)
            {
                _x0 += xa;
            }

            if (xa > 0.0F)
            {
                _x1 += xa;
            }

            if (ya < 0.0F)
            {
                _y0 += ya;
            }

            if (ya > 0.0F)
            {
                _y1 += ya;
            }

            return new AABB(_x0, _y0, _x1, _y1);
        }

        public float[] MoveCollide(World w, float xa, float ya)
        {

            AABB exp = this.Expand(xa, ya);

            int minX = (int)exp.x0 - 1;
            int maxX = (int)exp.x1 + 1;

            int minY = (int)exp.y0 - 1;
            int maxY = (int)exp.y1 + 1;

            for (int x = minX; x <= maxX; x++)
            {
                for (int y = minY; y <= maxY; y++)
                {

                    int blockID = w.GetBlock(x, y);
                    if (blockID > 0 && Blocks.GetBlock(blockID).Collidable)
                    {
                        xa = new AABB(x - 1, y - 1, x, y).ClipXCollide(this, xa);
                    }

                }
            }

            this.Move(xa, 0);

            for (int x = minX; x <= maxX; x++)
            {
                for (int y = minY; y <= maxY; y++)
                {

                    int blockID = w.GetBlock(x, y);
                    if (blockID > 0 && Blocks.GetBlock(blockID).Collidable)
                    {
                        ya = new AABB(x - 1, y - 1, x, y).ClipYCollide(this, ya);
                    }

                }
            }

            this.Move(0, ya);

            return new float[] { xa, ya };
        }

        public float ClipXCollide(AABB bb, float xa)
        {
            if (bb.y1 > this.y0 && bb.y0 < this.y1)
            {

                float max;
                if (xa > 0.0F && bb.x1 <= this.x0)
                {
                    max = this.x0 - bb.x1 - this.eps;
                    if (max < xa)
                    {
                        xa = max;
                    }
                }

                if (xa < 0.0F && bb.x0 >= this.x1)
                {
                    max = this.x1 - bb.x0 + this.eps;
                    if (max > xa)
                    {
                        xa = max;
                    }
                }

                return xa;
            }
            else
            {
                return xa;
            }
        }

        public float ClipYCollide(AABB bb, float ya)
        {
            if (bb.x1 > this.x0 && bb.x0 < this.x1)
            {
                float max;
                if (ya > 0.0F && bb.y1 <= this.y0)
                {
                    max = this.y0 - bb.y1 - this.eps;
                    if (max < ya)
                    {
                        ya = max;
                    }
                }

                if (ya < 0.0F && bb.y0 >= this.y1)
                {
                    max = this.y1 - bb.y0 + this.eps;
                    if (max > ya)
                    {
                        ya = max;
                    }
                }

                return ya;
            }
            else
            {
                return ya;
            }
        }

        public void Move(float xa, float ya)
        {
            this.x0 += xa;
            this.y0 += ya;
            this.x1 += xa;
            this.y1 += ya;
        }

        public bool Intersects(AABB c)
        {
            return (c.x1 > this.x0 && c.x0 < this.x1) ? (c.y1 > this.y0 && c.y0 < this.y1) : false;
        }
    }
}
