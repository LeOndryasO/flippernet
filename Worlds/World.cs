﻿using FlipperNet.Worlds.Entity;
using OpenTK;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace FlipperNet.Worlds
{
    public class World
    {
        private byte[,] blocks;
        public Game Game { get; private set; }

        public Color BackgroundTop { get; set; }
        public Color BackgroundBottom { get; set; }
        public Vector2 Size { get; set; }
        public Player Player { get; private set; }

        private List<BaseEntity> entities;
        public List<BaseEntity> Entities
        {
            get
            {
                return this.entities;
            }
        }

        public World(Game game, int width, int height)
        {
            this.Game = game;
            this.entities = new List<BaseEntity>();
            this.Size = new Vector2(width, height);
            this.blocks = new byte[width, height];
        }

        public int GetBlock(int x, int y)
        {
            if (x >= 0 && x < this.Size.X && y >= 0 && y < this.Size.Y)
            {
                return this.blocks[x, y];
            }

            return 0;
        }

        public void SetBlock(int x, int y, int id)
        {
            if (x >= 0 && x < this.Size.X && y >= 0 && y < this.Size.Y)
            {
                this.blocks[x, y] = (byte)id;
            }
        }

        public void AddEntity(BaseEntity e)
        {
            this.entities.Add(e);
        }

        public void CreatePlayer(int x, int y)
        {
            this.Player = new Player(x, y);
            this.entities.Add(this.Player);
        }

        public void BindPlayer(Player p)
        {
            this.Player = p;
            this.entities.Add(p);
        }

        public void UnbindPlayer()
        {
            this.entities.Remove(this.Player);
            this.Player = null;
        }

        public void Update()
        {
            foreach (BaseEntity e in this.entities)
            {
                e.Update(this);
            }

            this.entities.RemoveAll((e) => e.ShouldRemove());
        }
    }
}
