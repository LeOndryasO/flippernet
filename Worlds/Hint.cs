﻿using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;

using QuickFont;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace FlipperNet.Worlds
{
    public class Hint
    {
        private bool bothWorlds = false;
        private int fromX, toX;
        private ProcessedText text;
        private int wID;
        private Level level;
        private QFont font;
        private int textWidth;
        private int textHeight;

        private Color4 boundsColor = new Color4(0.25f, 0.25f, 0.25f, 1);
        private Color4 innerColor = new Color4(0.5f, 0.5f, 0.5f, 0.8f);

        private Vector2 temp = new Vector2();

        public Hint(String text, int fromX, int toX, Level level)
            : this(text, fromX, toX, level, -1)
        {
            this.bothWorlds = true;
        }

        public Hint(String text, int fromX, int toX, Level level, int worldId)
        {
            this.fromX = fromX;
            this.toX = toX;
            this.wID = worldId;
            this.level = level;
            this.font = level.GetWorld(0).Game.GetFontManager().GetFont(24);
            this.SetText(text, level.GetWorld(0).Game.Width);
        }

        public void SetText(String text, int windowWidth)
        {
            this.text = this.font.ProcessText(text, (windowWidth / 2 - 20), QFontAlignment.Left);
            
            SizeF m = this.font.Measure(this.text);
            this.textWidth = (int)m.Width;
            this.textHeight = (int)m.Height;
        }

        public void Render(World world, float ptt)
        {
            if (this.bothWorlds || this.level.GetWorld(this.wID) == world)
            {
                if (world.Player.BB.x1 > this.fromX
                        && world.Player.BB.x1 <= this.toX)
                {

                    int bs = world.Game.BlockSize;

                    int boxWidth = this.textWidth + 10;
                    int boxHeight = this.textHeight + 10;
                    int boxX = (int)((world.Player.RenderBB.x1 + 1.25) * bs);
                    int boxY = (int)((world.Player.RenderBB.y0 + 1.6) * bs);

                    this.temp.X = boxX + 5;
                    this.temp.Y = boxY + 7;

                    GLUtil.DisableEnableTex2D(() =>
                    {
                        this.RenderInner(boxX, boxX + boxWidth, boxY, boxY + boxHeight, this.innerColor);
                        this.RenderBounds(boxX, boxX + boxWidth, boxY, boxY + boxHeight, this.boundsColor);
                    });

                    this.font.Print(this.text, this.temp);

                }
            }
        }

        private void RenderBounds(float x1, float x2, float y1, float y2, Color4 color)
        {
            GL.LineWidth(1);
            GL.Begin(PrimitiveType.Lines);
            GL.Color4(color);
            this.Line(x1, x2, y1, y1);
            this.Line(x1, x1, y1, y2);
            this.Line(x2, x2, y1, y2);
            this.Line(x1, x2, y2, y2);
            GL.End();
        }

        private void Line(float x1, float x2, float y1, float y2)
        {
            GL.Vertex2(x1, y1);
            GL.Vertex2(x2, y2);
        }

        private void RenderInner(float x1, float x2, float y1, float y2, Color4 color)
        {
            GL.Begin(PrimitiveType.Quads);
            GL.Color4(color);
            GL.Vertex2(x1, y1);
            GL.Vertex2(x2, y1);
            GL.Vertex2(x2, y2);
            GL.Vertex2(x1, y2);
            GL.End();
        }
    }
}
