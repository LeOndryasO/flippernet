﻿using FlipperNet.Manager;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlipperNet
{
    public static class GLUtil
    {
        public static int colorTextureID;
        public static int framebufferID;
        public static int depthRenderBufferID;

        public static bool InitFBO(int width, int height)
        {
            bool ret = true;
            GL.Ext.GenFramebuffers(1, out GLUtil.framebufferID);
            GL.GenTextures(1, out GLUtil.colorTextureID);
            GL.Ext.GenRenderbuffers(1, out GLUtil.depthRenderBufferID);

            GL.BindTexture(TextureTarget.Texture2D, GLUtil.colorTextureID);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (int)TextureMinFilter.Nearest);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (int)TextureMagFilter.Nearest);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapS, (int)TextureWrapMode.Clamp);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapT, (int)TextureWrapMode.Clamp);
            GL.TexImage2D(TextureTarget.Texture2D, 0, PixelInternalFormat.Rgba8, width, height, 0, PixelFormat.Rgba, PixelType.Int, System.IntPtr.Zero);
            GL.BindTexture(TextureTarget.Texture2D, 0);

            GL.Ext.BindRenderbuffer(RenderbufferTarget.RenderbufferExt, GLUtil.depthRenderBufferID);
            GL.Ext.RenderbufferStorage(RenderbufferTarget.RenderbufferExt, RenderbufferStorage.DepthComponent24, width, height);

            GL.Ext.BindFramebuffer(FramebufferTarget.FramebufferExt, GLUtil.framebufferID);
            GL.Ext.FramebufferTexture2D(FramebufferTarget.FramebufferExt, FramebufferAttachment.ColorAttachment0Ext, TextureTarget.Texture2D, GLUtil.colorTextureID, 0);
            GL.Ext.FramebufferRenderbuffer(FramebufferTarget.FramebufferExt, FramebufferAttachment.DepthAttachmentExt, RenderbufferTarget.RenderbufferExt, GLUtil.depthRenderBufferID);

            FramebufferErrorCode e = GL.Ext.CheckFramebufferStatus(FramebufferTarget.FramebufferExt);
            if (e == FramebufferErrorCode.FramebufferCompleteExt)
            {
                Console.WriteLine("Framebuffer init succeded.");
            }
            else
            {
                Console.WriteLine("Framebuffer init FAILED!");

                try
                {
                    GL.DeleteTexture(GLUtil.colorTextureID);
                    GL.Ext.DeleteRenderbuffer(GLUtil.depthRenderBufferID);
                    GL.Ext.DeleteFramebuffer(GLUtil.framebufferID);
                }
                catch
                {
                    Console.WriteLine("Error cleanuping!");
                }

                ret = false;
            }


            GL.Ext.BindFramebuffer(FramebufferTarget.FramebufferExt, 0);
            GLUtil.SetOrtho(width, height);

            GL.Disable(EnableCap.Lighting);
            GL.Disable(EnableCap.DepthTest);

            return ret;
        }

        public static void SetOrtho(int width, int height)
        {
            GL.MatrixMode(MatrixMode.Projection);
            GL.LoadIdentity();
            GL.Ortho(0, width, height, 0, 0, 1);
            GL.MatrixMode(MatrixMode.Modelview);
        }

        public static void DrawTexture(TextureManager t, int texture, float textureX1,
            float textureX2, float textureY1, float textureY2, float x1,
            float x2, float y1, float y2)
        {
            t.Bind(texture);
            GL.Color4(1f, 1f, 1f, 1f);
            GL.Begin(PrimitiveType.Quads);
            GL.TexCoord2(textureX1, textureY1);
            GL.Vertex2(x1, y1);

            GL.TexCoord2(textureX2, textureY1);
            GL.Vertex2(x2, y1);

            GL.TexCoord2(textureX2, textureY2);
            GL.Vertex2(x2, y2);

            GL.TexCoord2(textureX1, textureY2);
            GL.Vertex2(x1, y2);
            GL.End();
        }

        public static void DrawTexture(TextureManager t, int texture, float x, float y)
        {
            Texture tex = t.GetTexture(texture);
            GLUtil.DrawTexture(t, texture, 0f, 1f, 0f, 1f, x, x + tex.Size.Width, y, y + tex.Size.Height);
        }

        public static void DrawAtlasTexture(TextureManager t, int texture, int image, float x1, float x2, float y1, float y2)
        {
            GLUtil.DrawTexture(t, texture, t.GetX1(texture, image), t.GetX2(texture, image), 0, 1, x1,
                x2, y1, y2);
        }

        public static void DrawAtlasTexture(TextureManager t, int texture, int image, float x, float y)
        {
            Vector2 size = t.GetTexture(texture).SheetSize;
            GLUtil.DrawAtlasTexture(t, texture, image, x, x + size.X, y, y + size.Y);
        }

        public static void DrawLine(float x1, float x2, float y1, float y2,
            int thickness, float r, float g, float b, float a)
        {
            GL.Color4(r, g, b, a);
            GL.LineWidth(thickness);

            GL.Begin(PrimitiveType.Lines);

            GL.Vertex2(x1, y1);
            GL.Vertex2(x2, y2);

            GL.End();
        }

        public static void DisableEnableTex2D(Action call)
        {
            GL.Disable(EnableCap.Texture2D);
            call();
            GL.Enable(EnableCap.Texture2D);
        }
    }
}
