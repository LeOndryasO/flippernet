﻿using FlipperNet.Worlds.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FlipperNet
{
    public class EntityCamera
    {
        public int TX { get; private set; }
        public int TY { get; private set; }
        public float OffsetX { get; set; }
        public float OffsetY { get; set; }

        private BaseEntity toAttach;
        private Game game;

        public EntityCamera(Game game, BaseEntity e, float oX, float oY)
        {
            this.game = game;
            this.toAttach = e;
            this.OffsetX = oX;
            this.OffsetY = oY;
        }

        public void Calculate()
        {
            int bs = this.game.BlockSize;

            float eCenterX = (this.toAttach.RenderBB.x1 - this.toAttach.RenderBB.x0) / 2f;
            float eCenterY = (this.toAttach.RenderBB.y1 - this.toAttach.RenderBB.y0) / 2f;

            int screenCenterX = this.game.Width / 2;
            int screenCenterY = this.game.Height / 2;

            this.TX = (int)((this.toAttach.RenderBB.x0 + eCenterX + OffsetX) * bs - screenCenterX);
            this.TY = (int)((this.toAttach.RenderBB.y0 + eCenterY + OffsetY) * bs - screenCenterY);
        }
    }
}
