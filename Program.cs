﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.IO;
using FlipperNet.Screen;
using FlipperNet.Worlds;
using System.Runtime.InteropServices;
using FlipperNet.Manager;

namespace FlipperNet
{
    class Program
    {
        [STAThread]
        static void Main(string[] args)
        {
            if (!UnpackNative())
            {
                Console.WriteLine("Could not start application!");
                Console.WriteLine("Please report contents of this window to ondryaso@ondryaso.eu");
                Console.ReadKey();
                Environment.Exit(1114);
            }

            ConfigManager m = new ConfigManager();
            int width = m.GetValueInt("width");
            int height = m.GetValueInt("height");
            int fps = m.GetValueInt("fps");
            bool c = m.GetValueBool("debug");

            using (var game = new Game(width, height, m))
            {
                if (c)
                {
                    Task.Factory.StartNew(() => CommandLoop(game));
                }
                else
                {
                    if (IsWin())
                    {
                        ShowWindow(GetConsoleWindow(), SW_HIDE);
                    }
                }

                if (fps != 0)
                    game.Run(fps);
                else
                    game.Run();               
            }
        }

        private static void CommandLoop(Game g)
        {
            Console.WriteLine("Starting command handler, write help for commands list");

            while (true)
            {
                String r = Console.ReadLine();
                if (r != null)
                {
                    String[] p = r.Split(' ');
                    if (p.Length >= 2)
                    {
                        try
                        {
                            switch (p[0])
                            {
                                case "strange_num":
                                    Game.STRANGE_NUM = int.Parse(p[1]);
                                    break;
                                case "ticks":
                                    Game.TICK_TIME = 1 / double.Parse(p[1]);
                                    break;
                                case "bs":
                                    g.BlockSize = int.Parse(p[1]);
                                    break;
                                case "lifes":
                                    ((GameScreen)g.GetScreen("game")).CurrentPlayer.Lifes = float.Parse(p[1]);
                                    break;
                                case "god":
                                    if (p[1] == "on")
                                    {
                                        Game.GodMode = true;
                                    }
                                    else if (p[1] == "off")
                                    {
                                        Game.GodMode = false;
                                    }
                                    break;
                                case "open":
                                    g.OpenScreen(p[1]);
                                    break;
                                case "show":
                                    switch (p[1])
                                    {
                                        case "lifes":
                                            Console.WriteLine("Lifes: " + ((GameScreen)g.GetScreen("game")).CurrentPlayer.Lifes);
                                            break;
                                        case "pos":
                                            AABB bb = ((GameScreen)g.GetScreen("game")).CurrentPlayer.BB;
                                            Console.WriteLine(String.Format("Position: X {0}, Y {1}", bb.x0, bb.y0));
                                            break;
                                        case "runtime":
                                            Console.WriteLine(String.Format("Running for {0} minutes and {1} seconds, thats {2} millis and {3} ticks",
                                                g.Stopwatch.Elapsed.Minutes, g.Stopwatch.Elapsed.Seconds, g.Stopwatch.ElapsedMilliseconds, g.TotalTicks));
                                            break;
                                    }
                                    break;
                            }
                        }
                        catch
                        {
                            Console.WriteLine("Error!");
                        }
                    }
                    else
                    {
                        Console.WriteLine("Usage: \n");
                        Console.WriteLine("strange_num [val/int]: Changes STRANGE_NUM, default 10000000");
                        Console.WriteLine("ticks [val/double]: Changes ticks per second, default 20");
                        Console.WriteLine("lifes [val/float]: Changes player lifes");
                        Console.WriteLine("bs [val/int]: Changes block size, will be saved");
                        Console.WriteLine("open [val/string]: Opens screen");
                        Console.WriteLine("show [lifes;pos;runtime]: You can figure out yourself");
                        Console.WriteLine("god [on;off]: Toggles god mode. Disables gravity and dying. Use S or down arrow to fly down.\nUse N to switch worlds. Use M to jump to the next level.");
                    }
                }
            }
        }

        private static bool UnpackNative()
        {
            string libDirPath = Path.Combine(Environment.CurrentDirectory, "Resources", "Lib");
            string libPath = Path.Combine(Environment.CurrentDirectory, "openal32.dll");
            string x86Path = Path.Combine(libDirPath, "oal32.dll");
            string x64Path = Path.Combine(libDirPath, "oal64.dll");

            if (IsWin())
            {
                try
                {
                    if (File.Exists(libPath))
                    {
                        File.Delete(libPath);
                    }

                    if (Isx64())
                    {
                        File.Copy(x64Path, libPath);
                    }
                    else
                    {
                        File.Copy(x86Path, libPath);
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine("Error unpacking libraries!");
                    Console.WriteLine(e.Message);
                    return false;
                }
            }

            return true;
        }

        private static bool IsWin()
        {
            return (Environment.OSVersion.Platform == PlatformID.Win32NT);
        }

        private static bool Isx64()
        {
            return (Environment.Is64BitOperatingSystem);
        }

        [DllImport("kernel32.dll")]
        static extern IntPtr GetConsoleWindow();

        [DllImport("user32.dll")]
        static extern bool ShowWindow(IntPtr hWnd, int nCmdShow);

        const int SW_HIDE = 0;
        const int SW_SHOW = 5;
    }
}

