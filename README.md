# Flipper.NET #

Flipper.NET is C# clone of our LD30 game, [Flipper](https://bitbucket.org/LeOndryasO/ld30)

## Dependencies ##

* [OpenTK](http://www.opentk.com/) 1.1
* [QuickFont](http://www.opentk.com/project/QuickFont) compiled for OpenTK 1.1
* [CGen.Audio](http://www.opentk.com/project/Cgaudio)

I didn't find QFont compiled for OpenTK 1.1, so I compiled it from source. You can use the compiled version from download.

### OpenAL ###

OpenTK tries to find OpenAL DLL only as "working dir/openal32.dll". There is little hack for multi-arch support, both libraries are in Resources/Lib and main method copies the right library just before the game itself is initialized. It is using [OpenAL Soft](http://kcat.strangesoft.net/openal.html)

## Complete downloads ##

* [Version R1](http://www.ondryaso.eu/ld30_csharp.zip)