﻿using OpenTK;
using QuickFont;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace FlipperNet.Screen
{
    public class AboutScreen : BaseScreen
    {
        private String[] lines;
        private QFont font;
        private float linesHeight;
        private float maxHeight;
        private float gameCenter;
        private SizeF measureTemp;
        private Vector2 temp;

        public AboutScreen(Game game)
            : base(game, "About")
        {
            this.gameCenter = (base.game.Width / 2);
            this.font = game.GetFontManager().GetFont();

            this.InitLines();
        }

        public override void Render(float ptt)
        {
            float y = ((base.game.Height / 2) - (this.linesHeight / 2));
            for (int i = 0; i < this.lines.Length; i++)
            {
                this.measureTemp = this.font.Measure(this.lines[i]);
                float x = (this.gameCenter - (measureTemp.Width / 2));

                this.temp.X = x;
                this.temp.Y = y;

                this.font.Print(this.lines[i], this.temp);
                y += this.maxHeight;
            }
        }

        public override void Update()
        {
        }

        public override void OnKeyDown(OpenTK.Input.KeyboardKeyEventArgs e)
        {
            if (e.Key == OpenTK.Input.Key.Escape)
            {
                base.game.GetSoundManager().PlaySound("select", 0.85f);
                base.game.OpenScreen("menu");
            }
        }

        private void InitLines()
        {
            this.lines = new String[] { "C# clone of the game developed for Ludum Dare 30 Jam", 
                "By LeOndryasO (a lot of original code by Dax105)", 
                "Using OpenTK, QFont and CGen.Audio"
				, "Most of the textures by Lars Doucet, Sean Choate", 
                " and Megan Bednarz - http://bit.ly/VKBqFh"
				, "Music from this awesome YT channel: ", "https://www.youtube.com/user/teknoaxe",
                "http://www.ludumdare.com/", 
                "https://bitbucket.org/LeOndryasO/flippernet", 
                "http://www.ondryaso.eu/"
				, "Made in the Czech Republic" };

            float max = 0;

            foreach (String l in this.lines)
            {
                float h = this.font.Measure(l).Height;
                if (h > max)
                    max = h;
            }

            this.maxHeight = max;
            this.linesHeight = this.lines.Length * maxHeight;
        }
    }
}
