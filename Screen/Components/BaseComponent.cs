﻿using OpenTK;
using OpenTK.Input;
using QuickFont;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace FlipperNet.Screen.Components
{
    internal abstract class BaseComponent
    {
        private const int SPACING = 10;

        protected Game game;
        protected ColumnComponentScreen compScreen;

        protected Vector2 position;
        protected Rectangle rect;

        public bool IsSelected { get; set; }
        public bool Enabled { get; set; }
        public bool Visible { get; set; }

        public Rectangle Rectangle
        {
            get
            {
                return this.rect;
            }
        }
        public int X
        {
            get
            {
                return this.rect.X;
            }

            set
            {
                this.rect.X = value;
                this.position.X = value;
            }
        }
        public int Y
        {
            get
            {
                return this.rect.Y;
            }

            set
            {
                this.rect.Y = value;
                this.position.Y = value;
            }
        }

        public BaseComponent(Game game, ColumnComponentScreen s, int x, int y)
        {
            this.game = game;
            this.compScreen = s;
            this.X = x;
            this.Y = y;
            this.Enabled = true;
            this.Visible = true;
        }

        public abstract void Update();
        public virtual void Render()
        {
            if (this.Visible)
            {
                if (this.IsSelected)
                {
                    GLUtil.DisableEnableTex2D(() =>
                    {
                        GLUtil.DrawLine(this.X - 5, this.X + this.Rectangle.Width + 5, this.Y + this.Rectangle.Height - BaseComponent.SPACING,
                            this.Y + this.Rectangle.Height - BaseComponent.SPACING, 3, 0.5f, 0.75f, 0.69f, 0.75f);
                    });
                }
            }
        }

        public virtual void KeyPress(KeyboardKeyEventArgs e)
        {
        }
    }
    internal class BasicComponent : BaseComponent
    {
        protected QFont font;
        protected int screenCenterX;
        protected bool centerX;

        private String text;
        public String Text
        {
            get
            {
                return this.text;
            }

            set
            {
                this.text = value;
                SizeF s = this.font.Measure(value);
                this.rect.Size = s.ToSize();

                if (this.centerX)
                {
                    this.X = this.screenCenterX - ((int)(s.Width / 2));
                }
            }
        }

        public QFont Font
        {
            get
            {
                return this.font;
            }

            set
            {
                this.font = value;
                this.Text = this.text;
            }
        }

        public Color TextColor { get; set; }

        public event Action OnActivation;

        public BasicComponent(Game g, ColumnComponentScreen s, String text, bool centerX) : base(g, s, 0, 0)
        {
            this.font = s.Font;
            this.screenCenterX = g.Width / 2;
            this.centerX = centerX;
            this.Text = text;
            this.TextColor = Color.White;
        }


        public override void KeyPress(OpenTK.Input.KeyboardKeyEventArgs e)
        {
            base.KeyPress(e);

            if (e.Key == Key.Enter || e.Key == Key.Space)
            {
                this.game.GetSoundManager().PlaySound("select", 1);
                if (this.OnActivation != null)
                {
                    this.OnActivation();
                }
            }
        }

        public override void Render()
        {
            if (this.Visible)
            {
                this.font.Options.Colour = this.TextColor;
                this.font.Print(this.Text, this.position);
            }
            base.Render();
        }

        public override void Update()
        {
        }
    }
}
