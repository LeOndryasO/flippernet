﻿using OpenTK.Input;
using QuickFont;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace FlipperNet.Screen.Components
{
    internal class ListComponent : BaseComponent
    {
        private String title;
        private String compl;

        private String[] items;
        private int currentItem;

        private QFont font;

        private int screenCenterX;
        private bool centerX;

        public String Title
        {
            get
            {
                return this.title;
            }

            set
            {
                this.title = value;
                this.compl = value + ": " + this.items[this.currentItem];
                this.Measure();
            }
        }

        public int CurrentItem
        {
            get
            {
                return this.currentItem;
            }

            set
            {
                if (value < this.items.Length)
                {
                    this.currentItem = value;
                    this.compl = this.title + ": " + this.items[value];
                    this.Measure();
                }
            }
        }

        public event Action OnValueChange;

        public ListComponent(Game game, ColumnComponentScreen s, String text, bool centerX, String[] items)
            : base(game, s, 0, 0)
        {
            this.title = text;
            this.items = items;
            this.currentItem = 0;
            this.font = s.Font;
            this.screenCenterX = game.Width / 2;
            this.centerX = centerX;

            this.compl = this.title + ": " + this.items[0];
            this.Measure();
        }

        public override void Render()
        {
            this.font.Print(this.compl, this.position);
            base.Render();
        }

        public override void KeyPress(OpenTK.Input.KeyboardKeyEventArgs e)
        {
            base.KeyPress(e);

            if (e.Key == Key.Left)
            {
                this.MoveSelected(true);
            }

            if (e.Key == Key.Right)
            {
                this.MoveSelected(false);
            }
        }

        public override void Update()
        {
        }

        private void Measure()
        {
            SizeF s = this.font.Measure(this.compl);
            this.rect.Size = s.ToSize();

            if (this.centerX)
            {
                this.X = this.screenCenterX - (int)(s.Width / 2);
            }
        }

        private void MoveSelected(bool minus)
        {
            int n = this.currentItem + (minus ? -1 : 1);

            if (n >= this.items.Length)
            {
                n = 0;
            }

            if (n < 0)
            {
                n = this.items.Length - 1;
            }

            this.CurrentItem = n;

            if (this.OnValueChange != null)
            {
                this.OnValueChange();
            }
        }
    }
}
