﻿using OpenTK.Input;
using QuickFont;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace FlipperNet.Screen.Components
{
    internal class BoolComponent : BaseComponent
    {
        private String _text;
        private String title;

        private QFont font;
        private int screenCenterX;
        private bool centerX;
        private bool val;

        public event Action OnValueChange;

        public bool Value
        {
            get
            {
                return this.val;
            }

            set
            {
                this.val = value;
                this.InnerText = this.title + ": " + this.BoolString(value);
            }
        }

        public String Text
        {
            get
            {
                return this.title;
            }

            set
            {
                this.title = value;
                this.InnerText = value + ": " + this.BoolString(this.val);
            }
        }

        public String InnerText
        {
            get
            {
                return this._text;
            }

            set
            {
                this._text = value;
                SizeF s = this.font.Measure(value);
                this.rect.Size = s.ToSize();

                if (this.centerX)
                {
                    this.X = this.screenCenterX - ((int)(s.Width / 2));
                }
            }
        }


        public BoolComponent(Game game, ColumnComponentScreen s, String text, bool centerX)
            : base(game, s, 0, 0)
        {
            this.font = s.Font;
            this.screenCenterX = game.Width / 2;
            this.centerX = centerX;
            this.Text = text;
        }

        private String BoolString(bool b)
        {
            return b ? "ON" : "OFF";
        }

        public override void Render()
        {
            this.font.Print(this.InnerText, this.position);
            base.Render();
        }

        public override void KeyPress(OpenTK.Input.KeyboardKeyEventArgs e)
        {
            base.KeyPress(e);

            if (e.Key == Key.Enter || e.Key == Key.Left || e.Key == Key.Right)
            {
                this.Value = !this.Value;
                if (this.OnValueChange != null)
                {
                    this.OnValueChange();
                }
            }
        }

        public override void Update()
        {
        }
    }
}
