﻿using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace FlipperNet.Screen
{
    public class ComingScreen : BaseScreen
    {
        String cumming = "Coming soon to your Windows";
        Vector2 temp = new Vector2();
        float a;
        int c = 1;
        Random rnd;
        Color4 color;
        SizeF s;

        public ComingScreen(Game game) : base(game, "How does the world look from the other side?")
        {
            this.rnd = new Random();
            this.color = new Color4((float)rnd.NextDouble(), 
                (float)rnd.NextDouble(), (float)rnd.NextDouble(), 1f);
           s = base.game.GetFontManager().GetFont().Measure(cumming);
        }

        private void BackRect()
        {
            int w = base.game.Width;
            int h = base.game.Height;
            GL.Color4(this.color);
            GL.Begin(PrimitiveType.Quads);
            GL.Vertex2(0, 0);
            GL.Vertex2(w, 0);
            GL.Vertex2(w, h);
            GL.Vertex2(0, h);
            GL.End();
        }

        public override void Render(float ptt)
        {
            this.BackRect();
            int w = 640;
            int h = 200;
            int x = (base.game.Width / 2) - (w / 2);
            int y = (base.game.Height / 2) - (h / 2);

            GLUtil.DrawTexture(base.game.GetTextureManager(), 0, 0f, 1f, 0f, 1f, x, x + w, y, y + h);
            GLUtil.DrawAtlasTexture(base.game.GetTextureManager(), 7, c, (x + w), (x + w + 200), y, (y + 200));

            
            w = (int)s.Width;
            h = (int)s.Height;

            temp.X = (base.game.Width / 2) - (w / 2);
            temp.Y = (base.game.Height) / 2 - (h / 2) + 120;

            base.game.GetFontManager().GetFont().Print(cumming, temp);
        }

        public override void Update()
        {
            a += 1;
            if (a >= 5)
            {
                c++;
                a = 0;

                this.color.R = (float)rnd.NextDouble();
                this.color.G = (float)rnd.NextDouble();
                this.color.B = (float)rnd.NextDouble();

                if (c > 9)
                    c = 2;
            }

            if (OpenTK.Input.Keyboard.GetState()[OpenTK.Input.Key.Enter])
                this.game.OpenScreen("menu");
        }
    }
}
