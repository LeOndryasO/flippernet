﻿using FlipperNet.Screen.Components;
using OpenTK.Input;
using QuickFont;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlipperNet.Screen
{
    public abstract class BaseScreen
    {
        public String Title { get; set; }
        protected Game game;
        private bool wasOpened;

        public BaseScreen(Game game, String title)
        {
            this.Title = title;
            this.game = game;
        }

        public bool WasOpened()
        {
            return this.wasOpened;
        }

        public virtual void OnOpen()
        {
            this.wasOpened = true;
        }

        public virtual void OnClose() { }

        public virtual void OnKeyDown(KeyboardKeyEventArgs e) { }

        public abstract void Render(float ptt);
        public abstract void Update();
    }

    public abstract class ColumnComponentScreen : BaseScreen
    {
        internal BaseComponent[] components;
        protected int lines = 0;
        protected int currentSelected = 0;

        protected QFont font;
        internal QFont Font
        {
            get
            {
                return this.font;
            }
        }

        public ColumnComponentScreen(Game game, String title)
            : base(game, title)
        {
            this.font = game.GetFontManager().GetFont(FlipperNet.Manager.FontManager.BIG);
        }

        public override void Render(float ptt)
        {
            foreach (BaseComponent c in this.components)
            {
                c.Render();
            }
        }

        public override void Update()
        {
            foreach (BaseComponent c in this.components)
            {
                c.Update();
            }
        }

        public override void OnKeyDown(OpenTK.Input.KeyboardKeyEventArgs e)
        {
            base.OnKeyDown(e);
            this.components[this.currentSelected].KeyPress(e);

            if (e.Key == Key.Up)
            {
                this.AddSelected(-1);
            }

            if (e.Key == Key.Down)
            {
                this.AddSelected(1);
            }
        }

        protected virtual void InitLines()
        {
            int all = 0;
            for (int i = 0; i < this.lines; i++)
            {
                all += this.components[i].Rectangle.Height;
            }

            int y = (this.game.Height / 2) - (all / 2);
            for (int i = 0; i < this.lines; i++)
            {
                this.components[i].Y = y;
                y += this.components[i].Rectangle.Height;
            }

            this.components[this.currentSelected].IsSelected = true;
        }

        protected void AddSelected(int i)
        {
            if (i != 0)
            {
                this.game.GetSoundManager().PlaySound("menu", 1);

                this.components[this.currentSelected].IsSelected = false;
                this.currentSelected += i;

                if (this.currentSelected >= this.lines)
                {
                    this.currentSelected = 0;
                }

                if (this.currentSelected < 0)
                {
                    this.currentSelected = this.lines - 1;
                }

                if (!this.components[this.currentSelected].Enabled)
                {
                    this.AddSelected(i < 0 ? -1 : 1);
                }

                this.components[this.currentSelected].IsSelected = true;
            }
        }
    }
}
