﻿using FlipperNet.Manager;
using OpenTK;
using QuickFont;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace FlipperNet.Screen
{
    public abstract class EndScreen : BaseScreen
    {
        protected bool canEscape = false;

        protected String mainLine = "";
        protected String secondLine = "";

        protected Vector2 main, second;
        protected QFont font, fontSm;

        private int timer;

        public EndScreen(Game game, String title)
            : base(game, title)
        {
            this.font = game.GetFontManager().GetFont(FontManager.BIG);
            this.fontSm = game.GetFontManager().GetFont(FontManager.SMALL);
        }


        public override void Render(float ptt)
        {
            this.font.Print(this.mainLine, this.main);

            if (this.canEscape)
            {
                this.fontSm.Print(this.secondLine, this.second);
            }
        }

        public override void Update()
        {
            if (!canEscape)
            {
                this.timer++;
                if (timer > 40)
                {
                    this.canEscape = true;
                }
            }
        }

        public override void OnKeyDown(OpenTK.Input.KeyboardKeyEventArgs e)
        {
            if ((e.Key == OpenTK.Input.Key.Escape) || (e.Key == OpenTK.Input.Key.Enter) || (e.Key == OpenTK.Input.Key.Space))
            {
                if (this.canEscape)
                {
                    this.game.OpenScreen("menu");
                }
            }
        }

        public override void OnOpen()
        {
            base.OnOpen();
            this.timer = 0;
            this.canEscape = false;
        }

        public override void OnClose()
        {
            this.game.RestartGame();
        }

        public void Measure()
        {
            float screenCenterX = this.game.Width / 2;
            float screenCenterY = this.game.Height / 2;

            SizeF mainSize = this.font.Measure(this.mainLine);

            this.main.X = screenCenterX - mainSize.Width / 2;
            this.main.Y = screenCenterY - mainSize.Height / 2;

            this.second.X = this.main.X;
            this.second.Y = this.main.Y + mainSize.Height;
        }
    }
}
