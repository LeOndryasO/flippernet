﻿using FlipperNet.Worlds;
using FlipperNet.Worlds.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenTK.Graphics.OpenGL;
using System.Drawing;
using FlipperNet.Manager;
using FlipperNet.Worlds.Block;
using OpenTK;

namespace FlipperNet.Screen
{
    public class GameScreen : BaseScreen
    {
        public const int PORTAL_TRANSITION_TICKS = 30;

        private WorldLoader loader;
        private World currentWorld;
        private Level currentLevel;
        private int levels, currentWorldID, currentLevelID;

        private EntityCamera camera;
        private TextureManager tm;

        public Player CurrentPlayer
        {
            get
            {
                return this.currentWorld.Player;
            }
        }

        private bool transitioning = false;
        public bool IsTransitioning
        {
            get
            {
                return this.transitioning;
            }
        }
        private int transTicks = 0;
        public int TransitionTicks
        {
            get
            {
                return this.transTicks;
            }
        }

        private bool isEnd = false;

        private int lifesX = 5, lifesY = 5;
        Vector2 coinsText;

        private int totalCoins = 0;
        public int Coins
        {
            get
            {
                return this.totalCoins;
            }
        }

        public GameScreen(Game game)
            : base(game, "Game")
        {
            this.tm = game.GetTextureManager();
            this.loader = game.Loader;
            this.loader.LoadLevels();
            this.currentLevel = loader.GetLevels()[0];
            this.currentWorld = this.currentLevel.GetWorld(0);
            this.levels = this.loader.GetLevels().Count;

            this.camera = new EntityCamera(game, this.currentWorld.Player, 1, 0);
            this.coinsText = new Vector2(this.lifesX + 160, this.lifesY - 1);
        }

        public override void Render(float ptt)
        {
            if (this.transitioning)
            {
                ptt = this.transTicks / GameScreen.PORTAL_TRANSITION_TICKS;
            }

            foreach (BaseEntity e in this.currentWorld.Entities)
            {
                e.BeforeRender(ptt);
            }

            this.camera.Calculate();
            GLUtil.DisableEnableTex2D(() =>
            {
                GL.Begin(PrimitiveType.Quads);
                GL.Color3(this.currentWorld.BackgroundTop);
                GL.Vertex2(0, 0);
                GL.Vertex2(this.game.Width, 0);
                GL.Color3(this.currentWorld.BackgroundBottom);
                GL.Vertex2(this.game.Width, this.game.Height);
                GL.Vertex2(0, this.game.Height);
                GL.End();
            });

            
            GL.PushMatrix();
            GL.Translate(-this.camera.TX, -this.camera.TY, 0);

            foreach (BaseEntity e in this.currentWorld.Entities)
            {
                e.Render(this.currentWorld, ptt);
            }

            GL.PopMatrix();

            int bs = this.game.BlockSize;

            for (int x = 0; x < this.currentWorld.Size.X; x++)
            {
                for (int y = 0; y < this.currentWorld.Size.Y; y++)
                {
                    if (this.currentWorld.GetBlock(x, y) > 0)
                    {
                        int x0 = -this.camera.TX + x * bs;
                        int x1 = -this.camera.TX + x * bs + bs;
                        int y0 = -this.camera.TY + y * bs;
                        int y1 = -this.camera.TY + y * bs + bs;

                        Block b = Blocks.GetBlock(this.currentWorld.GetBlock(x, y));
                        int tB = this.currentWorld.GetBlock(x, y - 1);
                        int bB = this.currentWorld.GetBlock(x, y + 1);
                        int lB = this.currentWorld.GetBlock(x - 1, y);
                        int rB = this.currentWorld.GetBlock(x + 1, y);

                        int texID = b.TextureID;
                        int sID = b.SpritesheetID;

                        #region Ugly texture messing
                        if (b == Blocks.grass)
                        {
                            if (tB == 0 && bB == 0 && lB == 0 && rB != 0)
                            {
                                sID = 13;
                            }
                            else if (tB == 0 && bB == 0 && rB == 0 && lB != 0)
                            {
                                sID = 15;
                            }
                            else if (tB != 0 && bB == 0 && lB == 0 && rB != 0)
                            {
                                sID = 4;
                            }
                            else if (tB != 0 && bB == 0 && rB == 0 && lB != 0)
                            {
                                sID = 6;
                            }
                            else if (tB == 0 && bB != 0 && lB == 0 && rB != 0)
                            {
                                sID = 1;
                            }
                            else if (tB == 0 && bB != 0 && rB == 0 && lB != 0)
                            {
                                sID = 3;
                            }
                            else if (tB == 0 && bB != 0 && rB != 0 && lB != 0)
                            {
                                sID = 2;
                            }
                            else if (tB != 0 && bB == 0 && rB != 0 && lB != 0)
                            {
                                sID = 5;
                            }
                            else if (tB == 0 && bB == 0 && rB != 0 && lB != 0)
                            {
                                sID = 14;
                            }
                            else if (tB == 0 && bB == 0 && rB == 0 && lB == 0)
                            {
                                sID = 16;
                            }
                            else if (tB == 0 && bB != 0 && rB == 0 && lB == 0)
                            {
                                sID = 17;
                            }
                            else if (tB != 0 && bB != 0 && rB == 0 && lB == 0)
                            {
                                sID = 18;
                            }
                            else if (tB != 0 && bB != 0 && rB != 0 && lB == 0)
                            {
                                sID = 19;
                            }
                            else if (tB != 0 && bB != 0 && rB == 0 && lB != 0)
                            {
                                sID = 20;
                            }
                            else
                            {
                                sID = 7;
                            }
                        }

                        if (b == Blocks.bouncy)
                        {
                            if (bB == 0)
                            {
                                sID = 23;
                            }
                        }
                        #endregion

                        GLUtil.DrawAtlasTexture(this.tm, texID, sID, x0, x1, y0, y1);
                    }
                }
            }

            GL.PushMatrix();
            GL.Translate(-this.camera.TX, -this.camera.TY, 0);

            foreach (Hint h in this.currentLevel.Hints)
            {
                h.Render(this.currentWorld, ptt);
            }

            GL.PopMatrix();

            this.DrawLifes();
            this.DrawCoins();
        }

        public override void OnOpen()
        {
            base.OnOpen();
            this.totalCoins = this.game.Config.GetValueInt("coins");
        }

        public override void OnClose()
        {
            this.game.Config.SetValue("coins", this.totalCoins);
        }

        public override void Update()
        {
            if (this.transitioning)
            {
                if (this.transTicks == GameScreen.PORTAL_TRANSITION_TICKS / 2)
                {
                    if (!this.isEnd)
                    {
                        Player p = this.currentWorld.Player;

                        this.currentWorldID++;
                        if (this.currentWorldID > 1)
                        {

                            this.currentWorldID = 0;
                        }

                        this.currentWorld.UnbindPlayer();
                        p.SetVelocityZero();
                        p.Lifes *= 1.5f;
                        this.currentWorld = this.currentLevel
                                .GetWorld(this.currentWorldID);
                        this.currentWorld.BindPlayer(p);
                    }
                    else
                    {
                        this.currentLevelID++;
                        if (this.currentLevelID == this.levels)
                        {
                            this.game.OpenScreen("win");
                            return;
                        }

                        this.currentLevel = loader.GetLevels()[this.currentLevelID];
                        this.currentWorldID = 0;
                        this.currentWorld = this.currentLevel.GetWorld(0);
                        this.camera = new EntityCamera(this.game, this.currentWorld.Player, 1,
                                0);
                    }
                }
                else if (this.transTicks >= GameScreen.PORTAL_TRANSITION_TICKS)
                {
                    this.transitioning = false;
                }

                this.transTicks++;
            }

            foreach (Block b in Blocks.GetAllBlocks())
            {
                if(b != null)
                    b.Update(this.currentWorld);
            }

            if (this.game.Keyboard[OpenTK.Input.Key.Escape])
            {
                this.game.OpenScreen("menu");
            }

            if (!this.transitioning)
            {
                this.currentWorld.Update();
            }

            this.totalCoins += this.currentWorld.Player.GetCoinsFlush();

            if (this.currentWorld.Player.ShouldRemove())
            {
                Console.WriteLine("When you play the Game of Thrones you win or you die, there is no middleground");
                this.game.OpenScreen("die");
            }

            if (!this.transitioning)
            {
                this.HandlePortal();
                this.HandleExit();
            }
        }

        private void DrawLifes()
        {
            TextureManager m = this.game.GetTextureManager();
            Texture tex = m.GetTexture(5);
            float l = (float)Math.Round(this.currentWorld.Player.Lifes, 2);

            GLUtil.DrawTexture(m, 6, this.lifesX, this.lifesY + 2);
            GLUtil.DrawTexture(m, 5, 0f, this.currentWorld.Player.Lifes, 0f, 1f, this.lifesX, 
                this.lifesX + tex.Size.Width * l, this.lifesY + 2, this.lifesY + 2 + tex.Size.Height);
        }

        
        private void DrawCoins()
        {
            GLUtil.DrawAtlasTexture(this.game.GetTextureManager(), 2, 5, this.lifesX + 130, this.lifesY - 5);
            this.game.GetFontManager().GetFont().Print(this.totalCoins.ToString(), this.coinsText);
        }

        private void HandlePortal()
        {
            if (this.currentWorld.Player.OnPortal)
            {
                this.transTicks = 0;
                this.transitioning = true;
                this.isEnd = false;

                this.game.GetSoundManager().PlaySound("portal", 1);
            }
        }

        private void HandleExit()
        {
            if (this.currentWorld.Player.OnExit)
            {
                this.transTicks = 0;
                this.transitioning = true;
                this.isEnd = true;

                this.game.GetSoundManager().PlaySound("portal", 0.8f);
            }
        }
    }
}
