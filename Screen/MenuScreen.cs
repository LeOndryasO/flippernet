﻿using FlipperNet.Manager;
using FlipperNet.Screen.Components;
using OpenTK;
using OpenTK.Graphics;
using OpenTK.Input;
using QuickFont;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace FlipperNet.Screen
{
    public class MenuScreen : ColumnComponentScreen
    {
        private QFont fontLg;
        private Texture logo;
        private int logoID = 0;
        private TextureManager m;
        private int screenCenterX;


        public MenuScreen(Game game)
            : base(game, "Game")
        {
            this.fontLg = game.GetFontManager().GetFont(FontManager.BIG);
            this.logo = game.GetTextureManager().GetTexture(this.logoID);
            this.m = game.GetTextureManager();
            this.screenCenterX = game.Width / 2;
            this.lines = 5;

            this.InitLines();
        }

        public override void Render(float ptt)
        {
            GLUtil.DrawTexture(m, this.logoID, (this.screenCenterX - (logo.Size.Width / 2)), 20);

            base.Render(ptt);
        }

        protected override void InitLines()
        {
            this.components = new BaseComponent[this.lines];

            ScreenOpenerComponent playGameComponent = new ScreenOpenerComponent(this.game, this, "Play game", "game");
            RestartGameComponent restartGameComponent = new RestartGameComponent(this.game, this);
            ScreenOpenerComponent optionsComponent = new ScreenOpenerComponent(this.game, this, "Options", "settings");
            ScreenOpenerComponent aboutComponent = new ScreenOpenerComponent(this.game, this, "About", "about");

            BasicComponent exitGameComponent = new BasicComponent(this.game, this, "Exit game", true);
            exitGameComponent.OnActivation += (() =>
            {
                this.game.GetSoundManager().StopMusic();
                System.Threading.Thread.Sleep(800);
                base.game.Exit();
            });

            this.components[0] = playGameComponent;
            this.components[1] = restartGameComponent;
            this.components[2] = optionsComponent;
            this.components[3] = aboutComponent;
            this.components[4] = exitGameComponent;

            base.InitLines();
        }


    }

    class RestartGameComponent : BasicComponent
    {
        public RestartGameComponent(Game g, MenuScreen m)
            : base(g, m, "Restart game", true)
        {
            this.OnActivation += RestartGameComponent_OnActivation;
        }

        private void RestartGameComponent_OnActivation()
        {
            this.game.RestartGame();
            this.game.OpenScreen("game");
        }

        public override void Update()
        {
            if (this.game.GetScreen("game").WasOpened())
            {
                this.TextColor = Color.White;
                this.Enabled = true;
            }
            else
            {
                this.TextColor = Color.Gray;
                this.Enabled = false;
            }

            base.Update();
        }
    }

    class ScreenOpenerComponent : BasicComponent
    {
        private String screen;

        public ScreenOpenerComponent(Game g, MenuScreen m, String text, String screenName)
            : base(g, m, text, true)
        {
            this.screen = screenName;
            this.OnActivation += ScreenOpenerComponent_OnActivation;
        }

        private void ScreenOpenerComponent_OnActivation()
        {
            this.game.OpenScreen(this.screen);
        }
    }
}
