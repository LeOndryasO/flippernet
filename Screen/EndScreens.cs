﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FlipperNet.Screen
{
    public class DieScreen : EndScreen
    {
        public DieScreen(Game game) : base(game, "Death")
        {
            this.mainLine = "You are dead :(";
            this.secondLine = "Return to menu";
            this.Measure();
        }
    }

    public class WinScreen : EndScreen
    {
        public WinScreen(Game game)
            : base(game, "Win")
        {
            this.mainLine = "You won... :)";
        }

        public override void OnOpen()
        {
            base.OnOpen();
            this.secondLine = "...and now you have " + ((GameScreen)game.GetScreen("game")).Coins + " coins :)";
            this.Measure();
        }
    }
}
