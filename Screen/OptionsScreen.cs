﻿using FlipperNet.Screen.Components;
using QuickFont;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using OpenTK.Input;

namespace FlipperNet.Screen
{
    public class OptionsScreen : ColumnComponentScreen
    {
        private String[] res = new String[] { "640x480", "800x600", "854x480", "1024x768", "1280x720", "1600x900", "1920x1080" };
        private BasicComponent changesComponent;

        public OptionsScreen(Game game)
            : base(game, "Options")
        {
            this.lines = 5;
            this.InitLines();
        }

        protected override void InitLines()
        {
            this.changesComponent = new BasicComponent(this.game, this, "You must restart the game for the changes to take effect", false);
            this.changesComponent.Font = this.game.GetFontManager().GetFont(24);
            this.changesComponent.Visible = false;
            this.changesComponent.X = (this.game.Width / 2) - (this.changesComponent.Rectangle.Width / 2);
            this.changesComponent.Y = (this.game.Height) - (this.changesComponent.Rectangle.Height);

            this.components = new BaseComponent[this.lines];

            BoolComponent sound = new BoolComponent(this.game, this, "Sound", true);
            sound.OnValueChange += (() => { this.game.GetSoundManager().SetSoundEnabled(sound.Value); });
            sound.Value = this.game.GetSoundManager().CanPlay;

            BoolComponent vsync = new BoolComponent(this.game, this, "VSync", true);
            vsync.OnValueChange += (() => { this.game.VSync = vsync.Value ? OpenTK.VSyncMode.On : OpenTK.VSyncMode.Off; });
            vsync.Value = (this.game.VSync == OpenTK.VSyncMode.On);

            BoolComponent console = new BoolComponent(this.game, this, "Game console", true);
            console.OnValueChange += (() =>
            {
                this.game.Config.SetValue("debug", console.Value);
                this.changesComponent.Visible = true;
            });
            console.Value = this.game.Config.GetValueBool("debug");

            ListComponent bs = new ListComponent(this.game, this, "Block size", true,
                new String[] { "Ultra small", "Normal", "Hardcore", "U can't see anythin'" });
            bs.OnValueChange += (() => this.ChangeBS(bs.CurrentItem));
            bs.CurrentItem = this.ResolveBS();

            ListComponent res = new ListComponent(this.game, this, "Resolution", true, this.res);
            res.OnValueChange += (() =>
            {
                this.ChangeRes(res.CurrentItem);
                this.changesComponent.Visible = true;
            });
            res.CurrentItem = this.ResolveRes();

            this.components[0] = sound;
            this.components[1] = bs;
            this.components[2] = res;
            this.components[3] = vsync;
            this.components[4] = console;

            base.InitLines();
        }

        public override void OnKeyDown(KeyboardKeyEventArgs e)
        {
            base.OnKeyDown(e);
            if (e.Key == Key.Escape)
            {
                this.game.OpenScreen("menu");
            }
        }

        public override void Render(float ptt)
        {
            base.Render(ptt);
            this.changesComponent.Render();
        }

        public override void Update()
        {
            base.Update();
        }

        private void ChangeRes(int v)
        {
            String[] p = this.res[v].Split('x');
            int w = int.Parse(p[0]);
            int h = int.Parse(p[1]);

            this.game.Config.SetValue("width", w);
            this.game.Config.SetValue("height", h);
        }
        private void ChangeBS(int v)
        {
            this.game.BlockSize = (int)Math.Pow(2, (v + 4));
        }
        private int ResolveBS()
        {
            switch (this.game.BlockSize)
            {
                case 16:
                    return 0;
                case 32:
                    return 1;
                case 64:
                    return 2;
                case 128:
                    return 3;
            }

            return -1;
        }
        private int ResolveRes()
        {
            String r = this.game.Width + "x" + this.game.Height;
            for (int i = 0; i < this.res.Length; i++)
            {
                if (this.res[i].Equals(r, StringComparison.InvariantCultureIgnoreCase))
                {
                    return i;
                }
            }

            return 2;
        }
    }
}
