﻿using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using OpenTK.Input;

using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

using FlipperNet.Manager;
using FlipperNet.Screen;
using System.Diagnostics;

namespace FlipperNet
{
    public class Game : GameWindow
    {
        #region Static
        public const String GAME_NAME = "Flipper.NET";
        public static double TICK_TIME = 1.0D / 20;
        public static int STRANGE_NUM = 10000000;
        public static bool GodMode = false;
        public int BlockSize = 32;
        public static Random rnd = new Random();
        #endregion

        #region Variables and properties
        //Measurement
        private Stopwatch sw;
        public Stopwatch Stopwatch
        {
            get
            {
                return this.sw;
            }
        }
        private long time, lastTime, lastInfo;
        private int ticks;
        private int lastTickMeasure;
        private long lastRenderTime;
        private int fps;

        private int? screenShader;
        private bool useFBO;
        private int timeUID, transitionUID;
        private int totalTicks;
        public int TotalTicks
        {
            get
            {
                return this.totalTicks;
            }
        }

        //FPS string
        private String fpsString = "FPS: 0";
        private Vector2 fpsPos;

        //Managers
        private FontManager fontManager;
        private SoundManager soundManager;
        private TextureManager textureManager;
        public ConfigManager Config { get; private set; }

        //Screens
        private BaseScreen currentScreen;
        private GameScreen gameScreen;
        private Dictionary<String, BaseScreen> screens;

        //World loader
        public WorldLoader Loader { get; private set; }
        #endregion

        public Game(int width, int height, ConfigManager c)
            : base(width, height)
        {
            base.Title = Game.GAME_NAME;
            base.Icon = FlipperNet.Properties.Resources.icon;

            this.Config = c;
            this.BlockSize = this.Config.GetValueInt("bs");

            this.fpsPos = new Vector2(width - 95, 5);
            this.sw = new Stopwatch();
        }

        #region Override methods
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            base.WindowBorder = OpenTK.WindowBorder.Fixed;

            //Init fps/ticks measurement
            this.sw.Start();
            this.lastRenderTime = 0;
            this.time = 0;
            this.lastInfo = 0;
            this.lastTime = 0;

            //Init GL
            this.useFBO = GLUtil.InitFBO(base.Width, base.Height);
            Console.WriteLine(this.useFBO ? "Using FBO" : "Not using FBO");
            GL.Enable(EnableCap.Blend);
            GL.BlendFunc(BlendingFactorSrc.SrcAlpha, BlendingFactorDest.OneMinusSrcAlpha);

            //Init shaders
            this.screenShader = ShaderLoader.loadShader("Resources/Shader/screen.vsh", "Resources/Shader/screen.fsh");
            this.timeUID = ShaderLoader.GetUniformID(this.screenShader, "time");
            this.transitionUID = ShaderLoader.GetUniformID(this.screenShader, "tI");

            //Init managers
            this.fontManager = new FontManager();
            this.soundManager = new SoundManager();
            this.textureManager = new TextureManager();
            this.Loader = new WorldLoader(this);

            //Load sound setting
            this.soundManager.SetSoundEnabled(this.Config.GetValueBool("sound"));

            //Load vsync setting
            base.VSync = this.Config.GetValueBool("vsync") ? VSyncMode.On : VSyncMode.Off;

            //Init screens
            this.screens = new Dictionary<string, BaseScreen>();
            this.LoadScreens();
            this.OpenScreen("menu");
        }

        protected override void OnUpdateFrame(FrameEventArgs e)
        {
            base.OnUpdateFrame(e);

#if DEBUG
            ErrorCode ec = GL.GetError();
            if (ec != ErrorCode.NoError)
                Console.WriteLine(ec.ToString());
#endif

            //Updates soundsystem
            this.soundManager.System.Update(e.Time * 1000);

            //Ticking
            this.time = this.sw.Elapsed.Ticks;
            while (this.time - this.lastTime >= Game.TICK_TIME * Game.STRANGE_NUM)
            {
                this.ticks++;
                this.Tick();
                this.lastTime += (long)(Game.TICK_TIME * Game.STRANGE_NUM);
            }

            if (this.time - this.lastInfo >= Game.STRANGE_NUM)
            {
                this.lastInfo += Game.STRANGE_NUM;
                this.lastTickMeasure = this.ticks;
                this.ticks = 0;
            }

            //Rendering
            float ptt = (this.time - this.lastTime) / ((float)(Game.TICK_TIME * Game.STRANGE_NUM));
            this.Render(ptt);
        }

        protected override void OnKeyDown(OpenTK.Input.KeyboardKeyEventArgs e)
        {
            base.OnKeyDown(e);
            //Pass key to current screen
            this.currentScreen.OnKeyDown(e);
        }

        protected override void OnClosing(System.ComponentModel.CancelEventArgs e)
        {
            base.OnClosing(e);

            //Save settings
            this.Config.SetValue("bs", this.BlockSize);
            this.Config.SetValue("sound", this.soundManager.CanPlay);
            this.Config.SetValue("vsync", (this.VSync == VSyncMode.On));
            this.Config.Save();

            this.currentScreen.OnClose();
            this.currentScreen = null;

            //Dispose managers
            this.soundManager.Dispose();
            this.textureManager.Dispose();
            this.fontManager.Dispose();
        }
        #endregion

        #region Private methods
        /// <summary>
        /// Base rendering method that handles rendering to FBO
        /// </summary>
        /// <param name="ptt">Partial tick time</param>
        private void Render(float ptt)
        {
            //Measure FPS
            this.fps++;

            if ((this.sw.Elapsed.Ticks - this.lastRenderTime) >= Game.STRANGE_NUM)
            {
                this.fpsString = "FPS: " + this.fps;
                this.fps = 0;
                this.lastRenderTime = this.sw.Elapsed.Ticks;
            }

            //Rendering
            if (this.useFBO)
            {
                //Bind FBO and call rendering method 
                GL.Ext.BindFramebuffer(FramebufferTarget.FramebufferExt, GLUtil.framebufferID);
                GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);
                this.DoRendering(ptt);

                //Bind screen and draw FBO
                GL.Ext.BindFramebuffer(FramebufferTarget.FramebufferExt, 0);
                GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);
                GL.Enable(EnableCap.Texture2D);

                //Apply shaders
                if (this.screenShader != null)
                {
                    GL.UseProgram((int)this.screenShader);
                    GL.Uniform1(this.timeUID, this.totalTicks + ptt);

                    if (this.currentScreen == this.gameScreen)
                    {
                        GameScreen gs = this.gameScreen;
                        if (gs.IsTransitioning)
                        {
                            int max = GameScreen.PORTAL_TRANSITION_TICKS;

                            float current = gs.TransitionTicks + ptt;
                            float dist = Math.Abs(max / 2 - current);
                            float tI = ((max / 2 - dist) * (100 / (max / 2f)) * 0.01f);

                            GL.Uniform1(this.transitionUID, Math.Max(0, tI));
                        }
                        else
                        {
                            GL.Uniform1(this.transitionUID, 0f);
                        }
                    }
                    else
                    {
                        GL.Uniform1(this.transitionUID, 0f);
                    }
                }

                //Draw FBO contents to the screen
                this.textureManager.ForceBindedID(GLUtil.colorTextureID);
                GL.BindTexture(TextureTarget.Texture2D, GLUtil.colorTextureID);
                GL.Color4(Color.White);
                GL.Begin(PrimitiveType.Quads);
                GL.TexCoord2(0f, 1f);
                GL.Vertex2(0f, 0f);
                GL.TexCoord2(1f, 1f);
                GL.Vertex2(base.Width, 0f);
                GL.TexCoord2(1f, 0f);
                GL.Vertex2(base.Width, base.Height);
                GL.TexCoord2(0f, 0f);
                GL.Vertex2(0f, base.Height);
                GL.End();

                GL.UseProgram(0);
                GL.Disable(EnableCap.Texture2D);
            }
            else //If card does not support FBOs
            {
                //Render directly to the screen (experimental)
                GL.Ext.BindFramebuffer(FramebufferTarget.FramebufferExt, 0);
                GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);

                this.DoRendering(ptt);
            }

            //Double-buffering
            base.SwapBuffers();
        }

        /// <summary>
        /// Method that renders the contents itself, called from Render
        /// </summary>
        /// <param name="ptt">Partial tick time</param>
        private void DoRendering(float ptt)
        {
            //Render current screen
            this.currentScreen.Render(ptt);
            //Print FPS string
            this.fontManager.GetFont().Print(this.fpsString, this.fpsPos);
        }

        /// <summary>
        /// The tick method itself, called from OnUpdateFrame in specified rate (TICK_TIME)
        /// </summary>
        /// <seealso cref="Game.TICK_TIME"/>
        private void Tick()
        {
            //Measurement
            this.totalTicks++;
            //Updates music
            this.soundManager.MusicManager.UpdateMusic(this.currentScreen == this.gameScreen);
            //Updates current screen
            this.currentScreen.Update();
        }
        #endregion

        #region Managers
        public TextureManager GetTextureManager()
        {
            return this.textureManager;
        }

        public FontManager GetFontManager()
        {
            return this.fontManager;
        }

        public SoundManager GetSoundManager()
        {
            return this.soundManager;
        }
        #endregion

        #region Screens
        /// <summary>
        /// Creates new Game screen and assigns it to the screens dictionary
        /// </summary>
        public void RestartGame()
        {
            this.gameScreen = new GameScreen(this);

            if (this.screens.ContainsKey("game"))
            {
                this.screens["game"] = this.gameScreen;
            }
            else
            {
                this.screens.Add("game", this.gameScreen);
            }
        }

        /// <summary>
        /// Loads all the screen. Should not be called
        /// </summary>
        public void LoadScreens()
        {
            this.screens.Add("coming", new ComingScreen(this));
            this.screens.Add("menu", new MenuScreen(this));
            this.screens.Add("about", new AboutScreen(this));
            this.screens.Add("win", new WinScreen(this));
            this.screens.Add("die", new DieScreen(this));
            this.screens.Add("settings", new OptionsScreen(this));
            this.RestartGame();
        }

        /// <summary>
        /// Gets screen from screens dictionary
        /// </summary>
        /// <param name="name">Screen key</param>
        /// <returns>Screen object (null if screen does not exist)</returns>
        public BaseScreen GetScreen(String name)
        {
            if (name == null)
                return null;

            BaseScreen s;
            this.screens.TryGetValue(name, out s);

            if (s != null)
                return s;

            return null;
        }

        /// <summary>
        /// Gets current opened screen
        /// </summary>
        /// <returns>Screen object (can be null)</returns>
        public BaseScreen GetCurrentScreen()
        {
            return this.currentScreen;
        }

        /// <summary>
        /// Opens screen and assignes it to the currentScreen variable
        /// </summary>
        /// <param name="screen">Screen object to open</param>
        public void OpenScreen(BaseScreen screen)
        {
            if (this.currentScreen != null)
            {
                this.currentScreen.OnClose();
            }

            screen.OnOpen();
            this.Title = Game.GAME_NAME + " - " + screen.Title;
            this.currentScreen = screen;
        }

        /// <summary>
        /// Opens screen from the screen dictionary
        /// </summary>
        /// <param name="name">Screen name</param>
        /// <exception cref="System.ArgumentException">Throws if specified screen does not exist</exception>
        public void OpenScreen(String name)
        {
            BaseScreen s = this.GetScreen(name);
            if (s == null)
            {
                throw new ArgumentException();
            }

            this.OpenScreen(s);
        }
        #endregion
    }
}
