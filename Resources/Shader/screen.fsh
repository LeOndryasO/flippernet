#version 120

//======================
//= EDITABLE VARIABLES =
//======================



//=============================
//= END OF EDITABLE VARIABLES =
//=============================

varying vec4 color;

uniform float time;
uniform float tI;

uniform sampler2D sampler;

uniform sampler2D tex;
uniform sampler2D tex2;
 
float radius = .5;

void main() {
    
    vec4 normalColor;
    
    vec2 m = vec2(0.5, 0.5);
	float d = distance(m, gl_TexCoord[0].st) * 0.8;
	vec3 c = texture2D(sampler, gl_TexCoord[0].st).rgb * (1.0 - d * d);
	//gl_FragColor = vec4(c.rgb, 1.0);
	normalColor = vec4(c.rgb, 1.0);
    
    
    float timeMult = time * 0.02;
   	
   	float t = clamp(timeMult / 6., 0., 1.);
 
	vec2 coords = gl_TexCoord[0].st;
	vec2 dir = coords - vec2(.5);
	
	float dist = distance(coords, vec2(.5));
	vec2 offset = dir * (sin(dist * 80. - timeMult*15.) + .5) / 5.;
 
	vec2 texCoord = coords + offset;
	vec4 diffuse = texture2D(tex, texCoord);
 
	vec4 mixin = texture2D(tex2, texCoord);
 

 	
 	gl_FragColor = mix((mixin * t + diffuse * (1. - t) + vec4(tI*0.5)) * vec4(1.0+tI*2), normalColor, 1.0-tI);
 	
    
    //vec2 texCoord = gl_TexCoord[0].st;
    //texCoord.s += sin(time*0.05)*0.02;
    //gl_FragColor = color * texture2D(sampler, texCoord);  
}