#version 120

//======================//
//= EDITABLE VARIABLES =//
//======================//



//=============================//
//= END OF EDITABLE VARIABLES =//
//=============================//

varying vec4 color;

uniform float time;

float rand(vec3 co){
    return fract(sin(dot(co.xz, vec2(12.9898,78.233))) * 43758.5453);
}

void main() {
	color.rgba = gl_Color.rgba;	

    gl_Position = gl_ModelViewProjectionMatrix * gl_Vertex;
    gl_TexCoord[0] = gl_MultiTexCoord0;
}